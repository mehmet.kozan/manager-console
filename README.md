# Redmine Metrik Konsol Uygulaması.
[Versiyonlar](https://gitlab.com/mehmet.kozan/manager-console/wikis/Versiyonlar)
## Redmine Rest Api kullanılarak c# dilinde yazıldı.
### Metrikler:
* M004: 
    * Metrik tipi: Proje.
    * İlgili Süreç Alanları: PMC-PP Proje İzleme ve Kontrol.
    * Hedef/Amaç: Proje yönetimine ayrılan sürenin belirlenmesi.
    * Soru: Proje yönetim eforu yeterli midir?
    * Metrik Adı: Proje Yönetimi Efor Metriği.
    * Metriği Toplayan Kişi: Proje Uzmanı.
    * Sorumlu Kişi: Proje Yöneticisi.
    * Hesaplama Yöntemi: (Proje Yönetim Eforu + Kalite Eforu + Konfigürasyon Eforu) / Toplam Efor.
    * Analiz Değerlendirme Kriteri: %18> PY Efor Metriği > %5.
    * Toplanma Sıklığı: 3 Aylık.
    * Analiz Sıklığı: Yıllık.
    * Kayıt Yeri: *PY Metriği *Metrik Havuzu.
    * Sunum Yeri: *Proje PADR.
    
* M005:
    * Metrik tipi: Proje.
    * İlgili Süreç Alanları: RSKM, Risk yönetimi.
    * Hedef/Amaç: Projedeki riskleri minimize etmek.
    * Soru: Proje risklerinin durumları nelerdir?
    * Metrik Adı: Risk Durum Metriği.
    * Metriği Toplayan Kişi: Proje Uzmanı.
    * Sorumlu Kişi: Proje Yöneticisi.
    * Hesaplama Yöntemi: Gerçekleşen Riskler / Tüm Riskler.
    * Analiz Değerlendirme Kriteri: Risk Durum Metriği < % 20.
    * Toplanma Sıklığı: Aylık.
    * Analiz Sıklığı: Aylık.
    * Kayıt Yeri: *PY Metriği *Metrik Havuzu.
    * Sunum Yeri: *Proje PADR.
    
* M006:
    * Metrik tipi: Proje.
    * İlgili Süreç Alanları: RSKM, Risk yönetimi.
    * Hedef/Amaç: Projedeki riskleri minimize etmek.
    * Soru: Proje risklerinin renk durumları nelerdir?
    * Metrik Adı: Risk Renk Metriği.
    * Metriği Toplayan Kişi: Proje Uzmanı.
    * Sorumlu Kişi: Proje Yöneticisi.
    * Hesaplama Yöntemi: Açık Risklerin dağılım durumları: Yeşil Risk Sayısı, Sarı Risk Sayısı, Kırmızı Risk Sayısı.
    * Analiz Değerlendirme Kriteri:  Açık Kırmızı Risk/Toplam Açık Risk < %20.
    * Toplanma Sıklığı: Aylık.
    * Analiz Sıklığı: Aylık.
    * Kayıt Yeri: *PY Metriği *Metrik Havuzu.
    * Sunum Yeri: *Proje PADR.
    
* M010:
    * Metrik tipi: Proje.
    * İlgili Süreç Alanları: VER, Gözden Geçirme.
    * Hedef/Amaç: Proje faaliyetlerindeki eksiklik ve hataların mümkün olduğunca erken tespit edip, düzeltmek.
    * Soru: Gözden geçirme faaliyetleri yeterli mi?
    * Metrik Adı: Gözden Geçirme Efor Metriği.
    * Metriği Toplayan Kişi: Kalite Uzmanı.
    * Sorumlu Kişi: Kalite Direktörü.
    * Hesaplama Yöntemi: Proje Gözden Geçirme Eforu / Proje Toplam Eforu.
    * Analiz Değerlendirme Kriteri:  Gözden Geçirme Efor Metriği > %5.
    * Toplanma Sıklığı: Aylık.
    * Analiz Sıklığı: Aylık.
    * Kayıt Yeri: *KG Metriği, *Metrik Havuzu
    * Sunum Yeri: *Kalite PADR.
    
* M013:
    * Metrik tipi: Proje.
    * İlgili Süreç Alanları: OPF, Düzeltici Önleyici Faaliyetler.
    * Hedef/Amaç: Düzeltici Önleyici Faaliyetleri takip etmek.
    * Soru: Düzeltici Önleyici Faaliyetleri takip ediliyor mu?
    * Metrik Adı: DÖF Metriği.
    * Metriği Toplayan Kişi: Kalite Uzmanı.
    * Sorumlu Kişi: Proje Yöneticisi.
    * Hesaplama Yöntemi: DÖF'lerin dağılım durumları: Yeni, Devam Ediyor, Çözüldü, Kapatıldı, Reddedildi, İptal.
    * Analiz Değerlendirme Kriteri:  Yeni durumundaki DÖF + Devam Ediyor durumundaki DÖF < 5.
    * Toplanma Sıklığı: Aylık.
    * Analiz Sıklığı: Aylık.
    * Kayıt Yeri: *KG Metriği, *Metrik Havuzu
    * Sunum Yeri: *Proje PADR, *Kalite PADR.