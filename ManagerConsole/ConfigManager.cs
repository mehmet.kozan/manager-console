﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    // static 
    public static class ConfigManager
    {
        /// <summary>
        /// Gets redmine accaount info (url, username, password).
        /// </summary>
        /// <returns>Account info (url, username, password) (string).</returns>
        public static RedmineUserAccount GetRedmineAccountInfo()
        {
            RedmineUserAccount redmineUserAccount = new RedmineUserAccount();

            var settings = ConfigurationManager.AppSettings;

            if (settings.AllKeys.Contains("Redmine Url"))
            {
                redmineUserAccount.RedmineUrl = settings["Redmine Url"];

                if (redmineUserAccount.RedmineUrl == "")
                {
                    Console.Error.WriteLine("\nLütfen config dosyasına host adresi giriniz.\n");
                    ConsoleManager.GetRedmineAccountInfo();
                    return null;
                }
                else
                {
                    redmineUserAccount.UserName = settings["Kullanıcı Adı"];
                    redmineUserAccount.Password = settings["Şifre"];

                    return redmineUserAccount;
                }
            }


            //settings.AllKeys.Contains("Redmine Url") && settings.AllKeys.Contains("Kullanıcı Adı") && settings.AllKeys.Contains("Şifre")
            //Console.Error.WriteLine("Config dosyası boş bir şekilde oluşturuldu.");            

            Console.Error.WriteLine("\nLütfen config dosyasına host adresi giriniz.\n");
            ConsoleManager.GetRedmineAccountInfo();
            return null;
        }



        /// <summary>
        /// If config file does not exist it creates config file without values.
        /// </summary>
        public static void ConfigCreateEmty()
        {
            if (ConfigurationManager.AppSettings == null)
            {
                ConfigXmlDocument configxmldoc = new ConfigXmlDocument();


                //var settings = ConfigurationManager.OpenExeConfiguration("ManagerConsole.exe");

            }
        }


        /// <summary>
        /// Config dosyası mevcut değilse boş bir config dosyası oluşturur.
        /// </summary>
        public static void CreateConfigIfNotExist()
        {
            if (!(File.Exists("ManagerConsole.exe.config")))
            {
                System.Text.StringBuilder sb = new StringBuilder();
                sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                sb.AppendLine("<configuration>");
                sb.AppendLine("<appSettings>");

                sb.AppendLine("<add key=\"Redmine Url\" value=\"\" />");
                sb.AppendLine("<add key=\"Kullanıcı Adı\" value=\"\" />");
                sb.AppendLine("<add key=\"Şifre\" value=\"\" />");

                sb.AppendLine("</appSettings>");
                sb.AppendLine("</configuration>");

                string loc = Assembly.GetEntryAssembly().Location;
                System.IO.File.WriteAllText(String.Concat(loc, ".config"), sb.ToString());
            }
        }

        public static void SaveAsFile(string str)
        {


            var appDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var filePath = System.IO.Path.Combine(appDir, FileName);

            System.IO.File.WriteAllText(filePath, str);


        }

        public static void SaveAsFile()
        {


            //string str = Serialize(this);
            string str = "";

            var appDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var filePath = System.IO.Path.Combine(appDir, FileName);

            System.IO.File.WriteAllText(filePath, str);


        }

        public static string FileName
        {
            get
            {
                return string.Format("ManagerConsole.exe.config");

            }

        }

        /// <summary>
        /// If config file does not exist it creates config file with values.
        /// </summary>
        /// <param name="redmineUserAccount">Redmine account info.</param>
        public static void ConfigCreate(RedmineUserAccount redmineUserAccount)
        {
            //it will be same as config create emty except values will be taken from console manager by calling this in console manager.
        }
    }
}
