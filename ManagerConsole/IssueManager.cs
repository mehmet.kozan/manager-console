﻿using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using ManagerConsole.MetricResults;

namespace ManagerConsole
{
    public class IssueManager
    {

        private RedmineManager redmineManager { get; set; }
        private IList<Tracker> trackerList { get; set; }
        private IList<IssueStatus> statusList { get; set; }
        private IList<TimeEntry> timeentryList { get; set; }
        private IList<TimeEntryActivity> activityList { get; set; }
        private IList<Issue> issueList { get; set; }


        public IssueManager(RedmineManager redmineManager)
        {

            this.redmineManager = redmineManager;

            var tracker_parameters = new NameValueCollection { 
                 { "tracker_id", "*" }
            
            };
            var status_parameters = new NameValueCollection { 
                 { "status_id", "*" }
            
            };
            var activity_parameters = new NameValueCollection { 
                 { "activity_id", "*" }
            
            };

            activityList = redmineManager.GetObjectList<TimeEntryActivity>(activity_parameters);
            trackerList = redmineManager.GetObjectList<Tracker>(tracker_parameters);
            statusList = redmineManager.GetObjectList<IssueStatus>(status_parameters);
        }


        /// <summary>
        /// Calls GetTimeEntryCountByActivity and gets total time entry hours by date and activity. And returns it to GetTimeEntryCount.
        /// </summary>
        /// <param name="pid">Project Id</param>
        /// <param name="name">Activity Name</param>
        /// <param name="i_month">Integer value for month.</param>
        /// <returns>Activiteye göre time entry saat toplamı (double)</returns>
        public double ReturnActivityCount(string pid, string name, DateTime? tarih)
        {
            double total = 0;
            if (name != "*")
            {
                foreach (var a in activityList.Where(t => t.Name.StartsWith(name)))
                {
                    total += GetTimeEntryCountByActivity(pid, a.Id.ToString(), tarih);
                }
            }
            else
            {
                total += GetTimeEntryCountByActivity(pid, "*", tarih);
            }


            return total;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="name"></param>
        /// <param name="tarih"></param>
        /// <returns></returns>
        public double ReturnActivityCount(string pid, string name)
        {
            double total = 0;
            if (name != "*")
            {
                foreach (var a in activityList.Where(t => t.Name.StartsWith(name)))
                {
                    total += GetTimeEntryCountByActivity(pid, a.Id.ToString());
                }
            }
            else
            {
                total += GetTimeEntryCountByActivity(pid, "*");
            }


            return total;
        }

        /// <summary>
        /// Gets tracker id by using name.
        /// </summary>
        /// <param name="name">Tracker Name</param>
        /// <returns>Tracker id (string)</returns>
        public string GetTrackerIdByName(string name)
        {
            string ret = string.Empty;

            if (trackerList.Where(t => t.Name == name).SingleOrDefault() != null)
            {
                ret = trackerList.Where(t => t.Name == name).SingleOrDefault().Id.ToString();

            }
            else
            {
                ret = null;
            }

            return ret;
        }

        /// <summary>
        /// Gets status id by using name.
        /// </summary>
        /// <param name="name">Status Name</param>
        /// <returns>Status id (string)</returns>
        public string GetStatusIdByName(string name)
        {
            string ret = string.Empty;

            if (statusList.Where(t => t.Name == name).SingleOrDefault() != null)
            {
                ret = statusList.Where(t => t.Name == name).SingleOrDefault().Id.ToString();
            }
            else
            {
                ret = null;
            }


            return ret;
        }

        /// <summary>
        /// Gets issue count as filtered by tracker.
        /// </summary>
        /// <param name="pid">Project ID.</param>
        /// <param name="tid">Tracker ID.</param>
        /// <returns>Tracker a göre issue sayısı (int)</returns>
        public int GetIssueCountByTracker(string pid, string tid)
        {
            int ret = 0;

            string tid_str = GetTrackerIdByName(tid);
            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", "*"},
                   { "tracker_id", tid_str}
                };

            redmineManager.GetObjectList<Issue>(parameters, out ret);


            Issue issue = new Issue();




            return ret;
        }

        /// <summary>
        /// Calls ReturnActivityCount gets total time entry hours as filtered by activity and date.
        /// </summary>
        /// <param name="pid">Project ID.</param>
        /// <param name="act">Activity Id</param>
        /// <param name="i_month">Integer value for month.</param>
        /// <returns>Activiteye göre time entry saat toplamı (double)</returns>
        public double GetTimeEntryCount(string pid, string act, DateTime? tarih)
        {
            double ret = 0;
            ret = ReturnActivityCount(pid, act, tarih);

            return ret;
        }

        /// <summary>
        /// Calls ReturnActivityCount gets total time entry hours.
        /// </summary>
        /// <param name="pid">Project ID</param>
        /// <param name="act">Activity ID</param>
        /// <returns></returns>
        public double GetTimeEntryCount(string pid, string act)
        {
            double ret = 0;
            ret = ReturnActivityCount(pid, act);

            return ret;
        }

        /// <summary>
        /// Gets time entry count as filtered by activity and date. And Returns it to ReturnActivityCount.
        /// </summary>
        /// <param name="pid">Project ID.</param>
        /// <param name="act">Activity ID.</param>
        /// <param name="i_month">Integer value for month.</param>
        /// <returns>Activiteye göre time entry saat toplamı (double)</returns>
        public double GetTimeEntryCountByActivity(string pid, string act, DateTime? tarih)
        {
            int ret = 0;
            double hours = 0;
            string date = DateStringCreator(tarih.Value.Month, tarih.Value.Year);

            if (act != "*")
            {
                var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   //{ "status_id", "*"},// kapalı silinmiş olanlar alınacak mı.
                   { "spent_on", date},
                   { "activity_id", act},
                   { "limit", "100"},
                   { "offset", "0"}
                };
                //--------
                timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters, out ret);

                int pageCount = (int)Math.Ceiling((double)ret / 100);

                Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
                {
                    parameters.Set("offset", (i * 100).ToString());
                    timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters);

                    foreach (var efor in timeentryList)
                    {
                        hours += (double)efor.Hours;
                    }
                });
            }
            else
            {
                foreach (var activity in activityList)
                {
                    var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "spent_on", date},
                   // {"status_id", "*"},// kapalı silinmiş olanlar alınacak mı.
                   {"activity_id",activity.Id.ToString()},
                   { "limit", "100"},
                   { "offset", "0"}
                };

                    timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters, out ret);

                    int pageCount = (int)Math.Ceiling((double)ret / 100);

                    Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
                    {
                        parameters.Set("offset", (i * 100).ToString());
                        timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters);

                        foreach (var efor in timeentryList)
                        {
                            hours += (double)efor.Hours;
                        }
                    });
                }
            }

            return hours;
        }

        /// <summary>
        /// Gets time entry count as filtered by activity and date.
        /// </summary>
        /// <param name="pid">project id</param>
        /// <param name="act">activity id</param>
        /// <returns>aktiviteye göre saat toplamı. (double)</returns>
        public double GetTimeEntryCountByActivity(string pid, string act)
        {
            int ret = 0;
            double hours = 0;

            if (act != "*")
            {
                var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   //{ "status_id", "*"},// kapalı silinmiş olanlar alınacak mı.
                   { "activity_id", act},
                   { "limit", "100"},
                   { "offset", "0"}
                };
                //--------
                timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters, out ret);

                int pageCount = (int)Math.Ceiling((double)ret / 100);

                Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
                {
                    parameters.Set("offset", (i * 100).ToString());
                    timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters);

                    foreach (var efor in timeentryList)
                    {
                        hours += (double)efor.Hours;
                    }
                });
            }
            else
            {
                //foreach (var activity in activityList)
                //{
                var parameters = new NameValueCollection { 
                   { "project_id",pid},
                  // {"status_id", "*"},// kapalı silinmiş olanlar alınacak mı.
                   //{"activity_id",activity.Id.ToString()},
                   { "limit", "100"},
                   { "offset", "0"}
                };

                timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters, out ret);

                int pageCount = (int)Math.Ceiling((double)ret / 100);

                Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
                {
                    parameters.Set("offset", (i * 100).ToString());
                    timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters);

                    foreach (var efor in timeentryList)
                    {
                        hours += (double)efor.Hours;
                    }
                });
                //}
            }

            return hours;
        }


        /// <summary>
        /// Gets tracker name by using tracker id.
        /// </summary>
        /// <param name="id">Tracker ID.</param>
        /// <returns>Tracker adı (string)</returns>
        public string GetTrackerNameById(string id)
        {

            string ret = string.Empty;

            ret = trackerList.Where(t => t.Id == int.Parse(id)).SingleOrDefault().Name.ToString();

            return ret;
        }

        /// <summary>
        /// Gets status name by using status id.
        /// </summary>
        /// <param name="id">Status ID.</param>
        /// <returns>Statu adı (string)</returns>
        public string GetStatusNameById(string id)
        {
            string ret = string.Empty;

            ret = statusList.Where(t => t.Id == int.Parse(id)).SingleOrDefault().Name.ToString();

            return ret;
        }

        /// <summary>
        /// Gets issue count as filtered by tracker, status.
        /// </summary>
        /// <param name="pid">Project ID.</param>
        /// <param name="tid">Tracker ID.</param>
        /// <param name="sid">Status ID.</param>
        /// <returns>Tracker ve statuye göre issue sayısı (integer)</returns>
        public int GetIssueCountByTrackerAndStatus(string pid, string tid, string sid)
        {
            int ret = 0;
            string sid_str;
            if (sid == "*" || sid == "")
            {
                sid_str = sid;
            }
            else
            {
                sid_str = GetStatusIdByName(sid);
            }
            string tid_str = GetTrackerIdByName(tid);

            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", sid_str},
                   { "tracker_id", tid_str}
                };
            // created on variable 

            if (sid_str == "")
            {
                parameters.Remove("status_id");
            }

            if (sid_str != null && tid_str != null)
            {
                redmineManager.GetObjectList<Issue>(parameters, out ret);
            }
            else
            {
                //Console.WriteLine("Tracker ya da status tipi bulunamadı.");
                ret = 0;
            }


            return ret;
        }

        /// <summary>
        /// Issueları statü, proje, tracker ve versiyonuna göre filtreleyip sonucu döner.
        /// </summary>
        /// <param name="pid">Project ID</param>
        /// <param name="tid">Tracker ID</param>
        /// <param name="sid">Status ID</param>
        /// <param name="versiyon_name">Version Name</param>
        /// <returns>Statü, Proje, Tracker ve Versiyona göre filtrelenmiş issue sayısı (int).</returns>
        public int GetIssueCountByTrackerStatusAndVersion(string pid, string tid, string sid)
        {
            int ret = 0;
            int totalcount = 0;
            string sid_str;
            if (sid == "*" || sid == "")
            {
                sid_str = sid;
            }
            else
            {
                sid_str = GetStatusIdByName(sid);
            }
            string tid_str = GetTrackerIdByName(tid);

            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", sid_str},
                   { "tracker_id", tid_str}
                };
            // created on variable 

            if (sid_str == "")
            {
                parameters.Remove("status_id");
            }

            IList<Redmine.Net.Api.Types.Version> versionlist = GetVersionsByProject(pid);

            if (sid_str != null && tid_str != null)
            {
                foreach (var version in versionlist)
                {
                    parameters.Remove("fixed_version_id");
                    parameters.Add("fixed_version_id", version.Id.ToString());
                    redmineManager.GetObjectList<Issue>(parameters, out totalcount);
                    ret += totalcount;
                }
            }
            else
            {
                //Console.WriteLine("Tracker ya da status tipi bulunamadı.");
                ret = 0;
            }

            return ret;
        }

        /// <summary>
        /// Proje id sine göre version listesini döner.
        /// </summary>
        /// <param name="pid">Project ID</param>
        /// <returns>Versions (list).</returns>
        public IList<Redmine.Net.Api.Types.Version> GetVersionsByProject(string pid)
        {
            var version_parameters = new NameValueCollection{
                {"project_id",pid}
            };
            IList<Redmine.Net.Api.Types.Version> versionlist = redmineManager.GetObjectList<Redmine.Net.Api.Types.Version>(version_parameters);

            return versionlist;
        }

        /// <summary>
        /// Gets issue count as filtered by tracker, status and date.
        /// </summary>
        /// <param name="pid">Project ID.</param>
        /// <param name="tid">Tracker ID.</param>
        /// <param name="sid">Status ID.</param>
        /// <param name="tarih">Date.</param>
        /// <returns>Issue sayısı (tracker ve statuye göre)</returns>
        public int GetIssueCountByTrackerAndStatus(string pid, string tid, string sid, DateTime? tarih)
        {
            int ret = 0;
            string sid_str;
            if (sid == "*" || sid == "")
            {
                sid_str = sid;
            }
            else
            {
                sid_str = GetStatusIdByName(sid);
            }
            string tid_str = GetTrackerIdByName(tid);
            string date = DateStringCreator(tarih.Value.Month, tarih.Value.Year);
            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", sid_str},
                   { "tracker_id", tid_str},
                   { "created_on", date}
                };
            // created on variable             

            if (sid_str == "")
            {
                parameters.Remove("status_id");
            }


            var iss = redmineManager.GetObjectList<Issue>(parameters, out ret);


            //updated on son update tarihini dönüyor.
            //var j = redmineManager.GetObjectList<Journal>(parameters);

            //foreach (var item in iss)
            //{
            //    Console.WriteLine("{0} {1} {2}", item.Id, item.UpdatedOn.Value, item.Journals);
            //    parameters.Set("issue_id", item.Id.ToString());
            //    j = redmineManager.GetObjectList<Journal>(parameters);
            //}


            return ret;
        }



        // spent hours and estimated hours. // returns null for all issues. spent time returns null.
        //todo çok fazla sürüyor, performansı iyileştirilmeli.
        public double GetIssueCountByTrackerAndStatus(string pid, string tid, string sid, string time, DateTime? tarih)
        {
            double ret = 0;
            double hour = 0;
            string sid_str;
            int count = 0;
            string sta = "status_id";
            if (sid == "*" || sid == "")
            {
                sid_str = sid;
            }
            else
            {
                sid_str = GetStatusIdByName(sid);
            }
            string tid_str = GetTrackerIdByName(tid);
            string date = DateStringCreator(tarih.Value.Month, tarih.Value.Year);
            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", sid_str},
                   { "tracker_id", tid_str},
                   { "created_on", date},
                   { "limit", "100"},
                   { "offset", "0"}
                };

            if (sid_str == "")
            {
                parameters.Remove(sta);
            }

            var issue_list = redmineManager.GetObjectList<Issue>(parameters, out count);
            int pageCount = (int)Math.Ceiling((double)count / 100);

            Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
            {
                parameters.Set("offset", (i * 100).ToString());
                issue_list = redmineManager.GetObjectList<Issue>(parameters);

                if (count != 0)
                {
                    if (time == "spent_time")
                    {
                        foreach (var iss in issue_list)
                        {
                            if (iss.SpentHours.HasValue) ret += (double)iss.SpentHours.Value;
                            parameters.Set("issue_id", iss.Id.ToString());
                            hour += GetTimeEntryCountByActivity(parameters);
                        }
                    }
                    else if (time == "estimated_time")
                    {
                        foreach (var iss in issue_list)
                        {
                            if (iss.EstimatedHours.HasValue) ret += (double)iss.EstimatedHours.Value;
                        }
                    }
                }
            });

            Console.WriteLine("{0}", hour);
            return ret;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid">project id</param>
        /// <param name="tid">tracker id</param>
        /// <param name="sid">status id</param>
        /// <returns></returns>
        //public double GetIssueCountByTrackerAndStatus(string pid, string tid, string sid)
        //{
        //    double ret = 0;
        //    double hour = 0;
        //    string sid_str;
        //    int count = 0;
        //    string sta = "status_id";
        //    if (sid == "*" || sid == "")
        //    {
        //        sid_str = sid;
        //    }
        //    else
        //    {
        //        sid_str = GetStatusIdByName(sid);
        //    }
        //    string tid_str = GetTrackerIdByName(tid);
        //    var parameters = new NameValueCollection { 
        //           { "project_id",pid},
        //           { "status_id", sid_str},
        //           { "tracker_id", tid_str},
        //           { "limit", "100"},
        //           { "offset", "0"}
        //        };

        //    if (sid_str == "")
        //    {
        //        parameters.Remove(sta);
        //    }

        //    var issue_list = redmineManager.GetObjectList<Issue>(parameters, out count);
        //    int pageCount = (int)Math.Ceiling((double)count / 100);

        //    Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
        //    {
        //        parameters.Set("offset", (i * 100).ToString());
        //        issue_list = redmineManager.GetObjectList<Issue>(parameters);
        //        hour += GetTimeEntryCountByActivity(parameters);
        //    });

        //    Console.WriteLine("{0}", hour);
        //    return ret;
        //}


        //time entry gereklilik--
        public double GetTimeEntryCountByActivity(NameValueCollection parameters)
        {
            int ret = 0;
            double hours = 0;

            timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters, out ret);

            int pageCount = (int)Math.Ceiling((double)ret / 100);

            Parallel.For(0, pageCount, new ParallelOptions { MaxDegreeOfParallelism = 10 }, i =>
            {
                parameters.Set("offset", (i * 100).ToString());
                timeentryList = redmineManager.GetObjectList<TimeEntry>(parameters);

                foreach (var efor in timeentryList)
                {
                    hours += (double)efor.Hours;
                }
            });

            return hours;
        }



        /// <summary>
        /// Gets issue count by using tracker, status, date and custom field filter.
        /// </summary>
        /// <param name="pid">Project ID</param>
        /// <param name="tid">Tracker ID</param>
        /// <param name="cfn">Custom Field Name</param>
        /// <param name="civ">Custom Field Value</param>
        /// <param name="sid">Status ID</param>
        /// <param name="tarih">Date</param>
        /// <returns>Issue sayısı (int)</returns>
        public int GetIssueCountByTrackerAndStatus(string pid, string tid, string cfn, string civ, string sid, DateTime? tarih)
        {

            int ret = 0;
            string sid_str;
            string tid_str = GetTrackerIdByName(tid);
            string sta = "status_id";
            if (sid == "*" || sid == "")
            {
                sid_str = sid;
            }
            else
            {
                sid_str = GetStatusIdByName(sid);
            }
            string date = DateStringCreator(tarih.Value.Month, tarih.Value.Year);

            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", sid_str},
                   { "tracker_id", tid_str},
                   { "created_on", date},
                };
            if (sid_str == "")
            {
                parameters.Remove(sta);
            }

            var issues = redmineManager.GetObjectList<Issue>(parameters, out ret);
            // created on variable 
            if (issues.Count != 0)
            {
                foreach (var item in issues)
                {
                    foreach (var custom in item.CustomFields)
                    {
                        if (cfn == custom.Name)
                        {
                            cfn = "cf_" + custom.Id.ToString();

                            break;
                        }
                    }
                    if (cfn.StartsWith("cf_"))
                    {
                        break;
                    }
                }

                parameters.Add(cfn, civ);
            }
            else
            {
                ret = 0;
                return ret;
            }


            issues = redmineManager.GetObjectList<Issue>(parameters, out ret);
            //custom field list.   

            return ret;
        }

        /// <summary>
        /// Gets issue count by using tracker, status, date and custom field filter.
        /// </summary>
        /// <param name="pid">project id</param>
        /// <param name="tid">tracker id</param>
        /// <param name="cfn">custom field name</param>
        /// <param name="civ">custom field value</param>
        /// <param name="sid">status id</param>
        /// <returns>issue sayısı.(int)</returns>
        public int GetIssueCountByTrackerAndStatus(string pid, string tid, string cfn, string civ, string sid)
        {

            int ret = 0;
            string sid_str;
            string tid_str = GetTrackerIdByName(tid);
            string sta = "status_id";
            if (sid == "*" || sid == "")
            {
                sid_str = sid;
            }
            else
            {
                sid_str = GetStatusIdByName(sid);
            }

            var parameters = new NameValueCollection { 
                   { "project_id",pid},
                   { "status_id", sid_str},
                   { "tracker_id", tid_str},
                };
            if (sid_str == "")
            {
                parameters.Remove(sta);
            }

            var issues = redmineManager.GetObjectList<Issue>(parameters, out ret);
            // created on variable 
            if (issues.Count != 0)
            {
                foreach (var item in issues)
                {
                    foreach (var custom in item.CustomFields)
                    {
                        if (cfn == custom.Name)
                        {
                            cfn = "cf_" + custom.Id.ToString();

                            break;
                        }
                    }
                    if (cfn.StartsWith("cf_"))
                    {
                        break;
                    }
                }

                parameters.Add(cfn, civ);
            }
            else
            {
                ret = 0;
                return ret;
            }


            issues = redmineManager.GetObjectList<Issue>(parameters, out ret);
            //custom field list.   

            return ret;
        }

        /// <summary>
        /// Creates Date string (><mindate|maxdate) for created_on to use as parameter in GetObjectList.
        /// </summary>
        /// <param name="i_month">Integer value for month.</param>
        /// <param name="year">Integer value for year.</param>
        /// <returns>Tarih aralığı (string)</returns>
        private string DateStringCreator(int i_month, int year)
        {
            string datemin, datemax, date, month_min, month_max, year_max, year_min;
            int i_month_min, i_month_max, i_year_min;

            //if (i_month < 10) month = "0" + i_month.ToString();

            if (i_month == 1)
            {
                i_month_min = 12;
                i_month_max = 2;
                year_max = year.ToString();
                year_min = (year - 1).ToString();
                i_year_min = (year - 1);
            }
            else if (i_month == 12)
            {
                i_month_min = 11;
                i_month_max = 1;
                year_max = (year + 1).ToString();
                year_min = year.ToString();
                i_year_min = year;
            }
            else
            {
                i_month_max = i_month + 1;
                i_month_min = i_month - 1;
                year_max = year.ToString();
                year_min = year.ToString();
                i_year_min = year;
            }


            if (i_month_max < 10)
            {
                month_max = "0" + i_month_max.ToString();
            }
            else
            {
                month_max = i_month_max.ToString();
            }


            if (i_month_min < 10)
            {
                month_min = "0" + i_month_min.ToString();
            }
            else
            {
                month_min = i_month_min.ToString();
            }

            datemin = year_min + "-" + month_min + "-" + DateTime.DaysInMonth(i_year_min, i_month_min);
            datemax = year_max + "-" + month_max + "-" + "01";

            date = "><" + datemin + "|" + datemax;

            return date;
        }


        /// <summary>
        /// Creates Date string (><mindate|maxdate) for created_on to use as parameter in GetObjectList.
        /// </summary>
        /// <param name="i_month">Integer value for month.</param>
        /// <returns>Tarih aralığı (string)</returns>
        private string DateStringCreator(int i_month)
        {
            string datemin, datemax, date, month_min, month_max, year_max, year_min;
            int i_month_min, i_month_max, i_year_min;
            int year = DateTime.Now.Year;

            //if (i_month < 10) month = "0" + i_month.ToString();

            if (i_month == 1)
            {
                i_month_min = 12;
                i_month_max = 2;
                year_max = year.ToString();
                year_min = (year - 1).ToString();
                i_year_min = (year - 1);
            }
            else if (i_month == 12)
            {
                i_month_min = 11;
                i_month_max = 1;
                year_max = (year + 1).ToString();
                year_min = year.ToString();
                i_year_min = year;
            }
            else
            {
                i_month_max = i_month + 1;
                i_month_min = i_month - 1;
                year_max = year.ToString();
                year_min = year.ToString();
                i_year_min = year;
            }


            if (i_month_max < 10)
            {
                month_max = "0" + i_month_max.ToString();
            }
            else
            {
                month_max = i_month_max.ToString();
            }


            if (i_month_min < 10)
            {
                month_min = "0" + i_month_min.ToString();
            }
            else
            {
                month_min = i_month_min.ToString();
            }

            datemin = year_min + "-" + month_min + "-" + DateTime.DaysInMonth(i_year_min, i_month_min);
            datemax = year_max + "-" + month_max + "-" + "01";

            date = "><" + datemin + "|" + datemax;

            return date;
        }
    }
}
