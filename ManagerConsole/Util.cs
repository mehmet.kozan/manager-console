﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    public static class Util
    {
        public static int GetIntFromUser(int min, int max)
        {
            int ret = -1;

            while (true)
            {
                Console.Write("\n>");
                string line = Console.ReadLine();

                bool result = int.TryParse(line, out ret);

                if (result)
                {
                    if (ret >= min && ret <= max)
                    {
                        return ret;
                    }
                    else
                    {
                        Console.Error.WriteLine("Lütfen {0} ile {1} arasında bir değer girin.", min, max);
                    }

                }
                else
                {
                    Console.Error.WriteLine("Please enter integer value");
                }
            }
        }



        public static int GetIntFromUser(string header, int min, int max)
        {
            int ret = -1;

            while (true)
            {
                Console.Write(string.Format("\n{0} >", header));
                string line = Console.ReadLine();

                bool result = int.TryParse(line, out ret);

                if (result)
                {
                    if (ret >= min && ret <= max)
                    {
                        return ret;
                    }
                    else
                    {
                        Console.Error.WriteLine("Lütfen {0} ile {1} arasında bir değer girin.", min, max);
                    }

                }
                else
                {
                    Console.Error.WriteLine("Please enter integer value");
                }
            }
        }

        public static string GetStringFromUser(string header)
        {
            Console.Write(string.Format("\n{0}\n>", header));
            return Console.ReadLine().Trim();
        }
    }
}
