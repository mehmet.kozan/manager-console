﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System.Configuration;
using System.Diagnostics;

namespace ManagerConsole
{
    class ServerManager
    {
        string Host { get; set; }
        string UName { get; set; }
        string Pass { get; set; }
        public RedmineManager RedmineManager { get; set; }
        public MetricManager MetricManager { get; set; }
        public ProjectManager ProjectManager { get; set; }


        int currentProjectId { get; set; }

        //public void Start()
        //{
        //    string metricName;
        //    string projectName;

        //    MetricManager metricmanager = new MetricManager(RedmineManager);
        //    ProjectManager projectManager = new ProjectManager(RedmineManager);

        //    List<string> projectNames = new List<string>();
        //    List<string> metricNames = new List<string>();

        //    foreach (var project in projectManager.Projects)
        //    {
        //        projectNames.Add(project.Name.ToString());
        //    }
        //    foreach (var metrics in metricmanager.MetricDictionary.Keys)
        //    {
        //        metricNames.Add(metrics.ToString());
        //    }

        //    metricName = ConsoleManager.GetMetricName(metricNames);
        //    projectName = ConsoleManager.GetProjectName(projectNames);

        //    Start(metricName, projectName);
        //}

        public void Start(string metricName, string projectName)
        {
            MetricManager.ExecuteMetric(metricName, projectName);
        }

        /// <summary>
        /// Kullanıcıda alınan bilgilerle server'a bağlan.
        /// </summary>
        public void ConnectServer(RedmineUserAccount redmineUserAccount)
        {
            RedmineManager = new RedmineManager(redmineUserAccount.RedmineUrl, redmineUserAccount.UserName, redmineUserAccount.Password);
            MetricManager = new MetricManager(RedmineManager);
            ProjectManager = new ProjectManager(RedmineManager);
        }
    }
}