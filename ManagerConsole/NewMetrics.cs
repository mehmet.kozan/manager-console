﻿using ManagerConsole.MetricResults;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    public partial class MetricManager
    {
        ////todo: eforun toplam sayısı dönmediği için her issuenunkini tek tek toplamak gerekiyor. Bu performans sorununa sebep oluyor.
        /// <summary>
        /// Proje yönetimi eforu, kalite eforu ve konfigürasyon efor toplamının, toplam efora oranını hesaplar.
        /// </summary>
        public void M004(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nProje Yönetimi Efor Metriği"));

            foreach (var project_id in projectId)
            {
                projectName = ProjectManager.GetProjectNameById(project_id);

                M004 m004 = new M004();

                m004.MetricName = "M004";
                m004.ProjecName = projectName;
                m004.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                double proje_yonetimi_eforu = IssueManager.GetTimeEntryCount(project_id, "Proje Yönetimi");
                double kalite_eforu = IssueManager.GetTimeEntryCount(project_id, "Kalite");
                double konfigurasyon_eforu = IssueManager.GetTimeEntryCount(project_id, "Konfigürasyon");
                double toplam_efor = IssueManager.GetTimeEntryCount(project_id, "*");

                m004.KaliteEforu = kalite_eforu;
                m004.ProjeYonetimEforu = proje_yonetimi_eforu;
                m004.KonfigurasyonEforu = konfigurasyon_eforu;
                m004.ToplamEfor = toplam_efor;

                stopwatch.Stop();
                m004.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m004.CalculateResult();

                m004.SaveAsFile();

                Console.WriteLine(m004);
            }
        }

        /// <summary>
        ///          Gerçekleşen risklerin tüm risklere oranını hesaplar.
        /// </summary>
        public void M005(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);

            Console.WriteLine(string.Format("\nRisk Durum Metriği"));

            foreach (var project_id in projectId)
            {
                M005 m005 = new M005();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m005.MetricName = "M005";
                m005.ProjecName = projectName;
                m005.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int gerceklesen_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Gerçekleşti");
                int tum_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "*");

                m005.GerceklesenRiskler = gerceklesen_riskler;
                m005.TumRiskler = tum_riskler;

                stopwatch.Stop();
                m005.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m005.CalculateResult();

                m005.SaveAsFile();
                Console.WriteLine(m005);
            }
        }

        /// <summary>
        /// Açık Kırmızı Risklerin Açık Tüm Risklere oranını bulur(cf_42).
        /// </summary>
        public void M006(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nRisk Renk Metriği"));

            foreach (var project_id in projectId)
            {
                M006 m006 = new M006();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m006.MetricName = "M006";
                m006.ProjecName = projectName;
                m006.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int yesil_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "Yeşil", "");
                int sari_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "Sarı", "");
                int kirmizi_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "Kırmızı", "");
                int tum_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "*", "");


                m006.YesilRiskler = yesil_riskler;
                m006.SariRiskler = sari_riskler;
                m006.KirmiziRiskler = kirmizi_riskler;
                m006.TumRiskler = tum_riskler;

                stopwatch.Stop();
                m006.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m006.CalculateResult();

                m006.SaveAsFile();

                Console.WriteLine(m006);
            }
        }

        /// <summary>
        /// Gözden geçirme eforunun toplam efora oranını hesaplar.
        /// </summary>
        public void M010(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGözden Geçirme Efor Metriği"));

            foreach (var project_id in projectId)
            {
                M010 m010 = new M010();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m010.MetricName = "M010";
                m010.ProjecName = projectName;
                m010.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                double gozden_gecirme_eforu = IssueManager.GetTimeEntryCount(project_id, "Gözden Geçirme");
                double toplam_efor = IssueManager.GetTimeEntryCount(project_id, "*");

                m010.GozdenGecirmeEforu = gozden_gecirme_eforu;
                m010.ToplamEfor = toplam_efor;

                stopwatch.Stop();
                m010.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m010.CalculateResult();

                m010.SaveAsFile();

                Console.WriteLine(m010);
            }
        }

        /// <summary>
        /// Yeni ve devam ediyor durumundaki döflerin toplamını hesaplar.
        /// </summary>
        public void M013(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nDÖF Metriği"));

            foreach (var project_id in projectId)
            {
                M013 m013 = new M013();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m013.MetricName = "M013";
                m013.ProjecName = projectName;
                m013.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Yeni");
                int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Devam Ediyor");
                int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Çözüldü");
                int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Kapatıldı");
                int reddedildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Reddedildi");
                int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "İptal");

                m013.Yeni = yeni;
                m013.DevamEdiyor = devam_ediyor;
                m013.Cozuldu = cozuldu;
                m013.Kapatildi = kapatildi;
                m013.Reddedildi = reddedildi;
                m013.Iptal = iptal;

                stopwatch.Stop();
                m013.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m013.CalculateResult();

                m013.SaveAsFile();

                Console.WriteLine(m013);
            }
        }


        /// <summary>
        /// Gereklilik trackerı içinde başlanmamış,devam eden, kodlanmış,biten statulerdeki issue sayılarını hesaplar.
        /// </summary>
        public void M014(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

            foreach (var project_id in projectId)
            {
                // proje id sinden ad .. 
                //projectName = ProjectManager.GetProjectNameById(project_id);

                M014 m014 = new M014();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m014.MetricName = "M014";
                m014.ProjecName = projectName;
                m014.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();


                int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Yeni");
                int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Devam Ediyor");
                int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Daha çok bilgi gerekiyor");
                int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Testten Geçemedi");
                int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü");
                int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)");
                int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
                int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
                int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

                int baslanmamis = yeni;
                int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
                int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                int biten = kapatildi + dogrulandi + iptal;


                m014.Baslanmamis = baslanmamis;
                m014.DevamEden = devam_eden;
                m014.Kodlanmis = kodlanmis;
                m014.Biten = biten;


                stopwatch.Stop();
                m014.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m014.SaveAsFile();

                Console.WriteLine(m014);
            }
        }


        //(Gereksinim Tamamlanma Metriği / (Gerçekleşen Efor / Planlanan Toplam Efor)) spent time returns null
        public void M015(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinim Tamamlanma Metriği: "));

            foreach (var project_id in projectId)
            {
                M015 m015 = new M015();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m015.MetricName = "M015";
                m015.ProjecName = projectName;
                m015.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                double planlanan_efor;
                Console.Write("Planlanan eforu girin:");
                planlanan_efor = double.Parse(Console.ReadLine());

                DateTime now = DateTime.Now;

                int toplam = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "*");
                int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
                int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
                int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

                double gerceklesen_efor = IssueManager.GetTimeEntryCount(project_id, "*");

                int biten = kapatildi + dogrulandi + iptal;


                m015.Toplam = toplam;
                m015.Biten = biten;

                m015.GerceklesenEfor = gerceklesen_efor;
                m015.PlanlananEfor = planlanan_efor;

                stopwatch.Stop();
                m015.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m015.CalculateResult();

                m015.SaveAsFile();

                Console.WriteLine(m015);
            }
        }


        /// <summary>
        /// Efor(Analiz + Tasarım + Geliştirme) / Gereksinim((Devam Eden * 0.5) + Kodlanmış + Biten) oranını hesaplar. (Gereksinim Eforu Metriği (saat/gereksinim) < ??)?.
        /// </summary>
        public void M016(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinim Eforu Metriği: "));

            foreach (var project_id in projectId)
            {
                M016 m016 = new M016();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m016.MetricName = "M016";
                m016.ProjecName = projectName;
                m016.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                double devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Devam Ediyor");
                double daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Daha çok bilgi gerekiyor");
                double testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Testten Geçemedi");
                double cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü");
                double tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)");
                double kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
                double dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
                double iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

                double devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
                double kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                double biten = kapatildi + dogrulandi + iptal;

                double analiz_eforu = IssueManager.GetTimeEntryCount(project_id, "Analiz");
                double tasarim_eforu = IssueManager.GetTimeEntryCount(project_id, "Tasarım");
                double gelistirme_eforu = IssueManager.GetTimeEntryCount(project_id, "Geliştirme");


                m016.AnalizEforu = analiz_eforu;
                m016.TasarimEforu = tasarim_eforu;
                m016.GelistirmeEforu = gelistirme_eforu;

                m016.DevamEden = devam_eden;
                m016.Kodlanmis = kodlanmis;
                m016.Biten = biten;

                stopwatch.Stop();
                m016.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m016.CalculateResult();

                m016.SaveAsFile();

                Console.WriteLine(m016);
            }
        }


        /// <summary>
        /// (Durum=İptal) + (Kaynağı=Ek Talep)(cf_31) / Toplam Gereksinim  oranını hesaplar.
        /// </summary>
        public void M017(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinimlerin Değişkenliği Metriği: "));

            foreach (var project_id in projectId)
            {
                M017 m017 = new M017();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m017.MetricName = "M017";
                m017.ProjecName = projectName;
                m017.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int ek_talep_iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kaynağı", "Ek Talep", "İptal");
                int toplam = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "*");

                m017.EkTalepIptal = ek_talep_iptal;
                m017.Toplam = toplam;

                stopwatch.Stop();
                m017.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m017.CalculateResult();

                m017.SaveAsFile();

                Console.WriteLine(m017);
            }
        }

        //roadmap = versiyon.
        /// <summary>
        /// RoadMap Gerçekleşme Metriği.
        /// </summary>
        public void M018(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nRoadMap Gerçekleşme Metriği: "));

            foreach (var project_id in projectId)
            {
                M018 m018 = new M018();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m018.MetricName = "M018";
                m018.ProjecName = projectName;
                m018.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int gereksinim = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "Gereksinim", "*");
                int hata = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "Hata", "*");
                int islem_maddesi = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "İşlem Maddesi", "*");
                int gorev = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "Görev", "*");

                m018.Gereksinim = gereksinim;
                m018.Hata = hata;
                m018.IslemMaddesi = islem_maddesi;
                m018.Gorev = gorev;

                stopwatch.Stop();
                m018.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m018.CalculateResult();

                m018.SaveAsFile();

                Console.WriteLine(m018);
            }
        }


        /// <summary>
        /// Kullanım durumu metriği.
        /// </summary>
        public void M019(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

            foreach (var project_id in projectId)
            {
                M019 m019 = new M019();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m019.MetricName = "M019";
                m019.ProjecName = projectName;
                m019.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int ilk_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "İlk KD");
                int degisen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Değişen KD");
                int eklenen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Eklenen KD");
                int cikarilan_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Çıkarılan KD");
                int silinen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Silinen KD");

                m019.Ilk = ilk_kd;
                m019.Degisen = degisen_kd;
                m019.Eklenen = eklenen_kd;
                m019.Cıkartılan = cikarilan_kd;
                m019.Silinen = silinen_kd;

                stopwatch.Stop();
                m019.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m019.SaveAsFile();

                Console.WriteLine(m019);
            }
        }

        /// <summary>
        /// Kullanım Durumunun Değişkenliği Metriği.
        /// </summary>
        public void M020(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

            foreach (var project_id in projectId)
            {
                M020 m020 = new M020();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m020.MetricName = "M020";
                m020.ProjecName = projectName;
                m020.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int ilk_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "İlk KD");
                int degisen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Değişen KD");
                int eklenen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Eklenen KD");
                int cikarilan_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Çıkarılan KD");
                int silinen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Silinen KD");

                m020.Ilk = ilk_kd;
                m020.Degisen = degisen_kd;
                m020.Eklenen = eklenen_kd;
                m020.Cikartilan = cikarilan_kd;
                m020.Silinen = silinen_kd;

                stopwatch.Stop();
                m020.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m020.CalculateResult();

                m020.SaveAsFile();

                Console.WriteLine(m020);
            }
        }


        /// <summary>
        /// Toplam MDK Sayısını hesaplar.
        /// </summary>
        public void M021(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

            foreach (var project_id in projectId)
            {
                M021 m021 = new M021();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m021.MetricName = "M021";
                m021.ProjecName = projectName;
                m021.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int acik_mdk = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Mühendislik Değişiklik Kaydı", "");
                int kapali_mdk = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Mühendislik Değişiklik Kaydı", "Kapatıldı");

                m021.AcikMDK = acik_mdk;
                m021.KapaliMDK = kapali_mdk;

                stopwatch.Stop();
                m021.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m021.CalculateResult();

                m021.SaveAsFile();

                Console.WriteLine(m021);
            }
        }

        /// <summary>
        /// (Toplam Hata Sayısı / Kodlanmış + Biten Gereksinim Sayısı)  oranını hesaplar. (/gereksinim?)
        /// </summary>
        public void M022(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nHata Dağılımı Metriği: "));

            foreach (var project_id in projectId)
            {
                M022 m022 = new M022();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m022.MetricName = "M022";
                m022.ProjecName = projectName;
                m022.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Yeni");
                int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Devam Ediyor");
                int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Daha çok bilgi gerekiyor");
                int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Testten Geçemedi");
                int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Çözüldü");
                int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Tamamlandı (Versiyona hazır)");
                int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Kapatıldı");
                int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Doğrulandı");
                int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "İptal");

                int cozuldu_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü");
                int tamamlandi_versiyona_hazir_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)");
                int kapatildi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
                int dogrulandi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
                int iptal_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

                int baslanmamis = yeni;
                int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
                int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                int biten = kapatildi + dogrulandi + iptal;

                int kodlanmis_gereksinim = cozuldu_gereksinim + tamamlandi_versiyona_hazir_gereksinim;
                int biten_gereksinim = kapatildi_gereksinim + dogrulandi_gereksinim + iptal_gereksinim;


                m022.Baslanmamis = baslanmamis;
                m022.DevamEden = devam_eden;
                m022.Kodlanmis = kodlanmis;
                m022.Biten = biten;

                m022.KodlanmisGereksinim = kodlanmis_gereksinim;
                m022.BitenGereksinim = biten_gereksinim;

                stopwatch.Stop();
                m022.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m022.CalculateResult();

                m022.SaveAsFile();

                Console.WriteLine(m022);
            }
        }


        /// <summary>
        /// (Hata(Önem=1) / Gereksinim( Kodlanmış + Biten)) < 0.2 oranını hesaplar. (gereksinim?)
        /// </summary>
        public void M023(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nAçık Hata Önem Dağılımı Metriği: "));

            foreach (var project_id in projectId)
            {
                M023 m023 = new M023();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m023.MetricName = "M023";
                m023.ProjecName = projectName;
                m023.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int acik_hata_onem_1 = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "");

                int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Yeni");

                int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Devam Ediyor");
                int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Daha çok bilgi gerekiyor");
                int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Testten Geçemedi");
                int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Çözüldü");
                int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Tamamlandı (Versiyona hazır)");

                int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;

                acik_hata_onem_1 = kodlanmis + devam_eden + yeni;

                cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Çözüldü");
                tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Tamamlandı (Versiyona hazır)");
                int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Kapatıldı");
                int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Doğrulandı");
                int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "İptal");

                int cozuldu_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü");
                int tamamlandi_versiyona_hazir_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)");
                int kapatildi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
                int dogrulandi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
                int iptal_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");


                kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                int biten = kapatildi + dogrulandi + iptal;

                int kodlanmis_gereksinim = cozuldu_gereksinim + tamamlandi_versiyona_hazir_gereksinim;
                int biten_gereksinim = kapatildi_gereksinim + dogrulandi_gereksinim + iptal_gereksinim;


                m023.AcikHataOnem1 = acik_hata_onem_1;
                m023.Kodlanmis = kodlanmis;
                m023.Biten = biten;

                m023.KodlanmisGereksinim = kodlanmis_gereksinim;
                m023.BitenGereksinim = biten_gereksinim;

                int acik_hata_onem_2 = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "");
                int acik_hata_onem_3 = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "3", "");
                int acik_hata_onem_4 = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "4", "");
                int acik_hata_onem_5 = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "5 (En Önemsiz)", "");

                m023.AcikHataOnem2 = acik_hata_onem_2;
                m023.AcikHataOnem3 = acik_hata_onem_3;
                m023.AcikHataOnem4 = acik_hata_onem_4;
                m023.AcikHataOnem5 = acik_hata_onem_5;


                stopwatch.Stop();
                m023.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m023.CalculateResult();

                m023.SaveAsFile();


                Console.WriteLine(m023);
            }
        }


        /// <summary>
        /// #(Yaş(Hata(Önem=1, 2))>30 gün) = 0 eşitliğini sorgular.
        /// </summary>
        public void M024(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nAçık Hata Yaşı Metriği: "));

            foreach (var project_id in projectId)
            {
                M024 m024 = new M024();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m024.MetricName = "M024";
                m024.ProjecName = projectName;
                m024.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Yeni");
                int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Devam Ediyor");
                int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Daha çok bilgi gerekiyor");
                int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Testten Geçemedi");
                int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Çözüldü");
                int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Tamamlandı (Versiyona hazır)");

                int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;

                int acik_hata_onem_1 = kodlanmis + devam_eden + yeni;

                yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Yeni");
                devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Devam Ediyor");
                daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Daha çok bilgi gerekiyor");
                testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Testten Geçemedi");
                cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Çözüldü");
                tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Tamamlandı (Versiyona hazır)");

                kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
                devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;

                int acik_hata_onem_2 = kodlanmis + devam_eden + yeni;


                m024.AcikHataOnem1 = acik_hata_onem_1;
                m024.AcikHataOnem2 = acik_hata_onem_2;

                stopwatch.Stop();
                m024.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m024.CalculateResult();

                m024.SaveAsFile();

                Console.WriteLine(m024);
            }
        }


        /// <summary>
        /// * Açık Hata-> Başlanmamış+Devam Eden-Hata Değil ve * Kapalı Hata + Hata Değil-> Biten+Hata Değil değerlerini hesaplar.
        /// </summary>
        public void M025(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nHata Durumu Metriği: "));

            foreach (var project_id in projectId)
            {
                M025 m025 = new M025();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m025.MetricName = "M025";
                m025.ProjecName = projectName;
                m025.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Yeni");
                int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Devam Ediyor");
                int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Daha çok bilgi gerekiyor");
                int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Testten Geçemedi");
                int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Kapatıldı");
                int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Doğrulandı");
                int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "İptal");

                int baslanmamis = yeni;
                int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
                int biten = kapatildi + dogrulandi + iptal;

                int hata_degil = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Değil");

                m025.Baslanmamis = baslanmamis;
                m025.DevamEden = devam_eden;
                m025.Biten = biten;
                m025.HataDegil = hata_degil;

                stopwatch.Stop();
                m025.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m025.CalculateResult();

                m025.SaveAsFile();

                Console.WriteLine(m025);
            }
        }



        /// <summary>
        /// Dış Kaynaklı Hata Sayısı < İç Kaynaklı Hata Sayısı (hata kaynağı=Bulgu Kaynağı?)
        /// </summary>
        public void M026(string projectName)
        {
            List<string> projectId = ProjectManager.GetProjectIdByName(projectName);
            Console.WriteLine(string.Format("\nİç/Dış Kaynaklı Hata Metriği: "));

            foreach (var project_id in projectId)
            {
                M026 m026 = new M026();

                projectName = ProjectManager.GetProjectNameById(project_id);

                m026.MetricName = "M026";
                m026.ProjecName = projectName;
                m026.ExecuteDate = DateTime.Now;
                Stopwatch stopwatch = Stopwatch.StartNew();

                int dis_kaynakli_hata = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Bulgu Kaynağı", "Dış Kaynaklı", "*");
                int ic_kaynakli_hata = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Bulgu Kaynağı", "İç Kaynaklı", "*");

                m026.DisKaynakliHataSayisi = dis_kaynakli_hata;
                m026.IcKaynakliHataSayisi = ic_kaynakli_hata;

                stopwatch.Stop();
                m026.TotatExecutionTime = stopwatch.Elapsed.TotalSeconds;

                m026.CalculateResult();

                m026.SaveAsFile();

                Console.WriteLine(m026);
            }
        }


    }
}
