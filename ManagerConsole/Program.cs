﻿using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Xml;
using ManagerConsole.MetricResults;
using System.Configuration;
using System.Reflection;


namespace ManagerConsole
{
    class Program
    {
        public static string[] argumans { get; set; }

        public static void Main(string[] args)
        {
            argumans = args;

            ConfigManager.CreateConfigIfNotExist();

            ServerManager ServerManager = new ServerManager();

            RedmineUserAccount redmineUserAccount;
            string metricName, projectName;

            if (args.Length >= 2)
            {
                redmineUserAccount = ConfigManager.GetRedmineAccountInfo();

                metricName = args[0];
                projectName = args[1];

                ServerManager.ConnectServer(redmineUserAccount);
                ServerManager.Start(metricName, projectName);
            }
            else
            {
                redmineUserAccount = ConsoleManager.GetRedmineAccountInfo();
                if (redmineUserAccount != null)
                {
                    ServerManager.ConnectServer(redmineUserAccount);
                    //ServerManager.Start();                

                    List<string> projectNames = new List<string>();
                    List<string> metricNames = new List<string>();

                    foreach (var project in ServerManager.ProjectManager.Projects)
                    {
                        projectNames.Add(project.Name.ToString());
                    }
                    foreach (var metrics in ServerManager.MetricManager.MetricDictionary.Keys)
                    {
                        metricNames.Add(metrics.ToString());
                    }
                    while (true)
                    {
                        metricName = ConsoleManager.GetMetricName(metricNames);
                        if (metricName == null)
                            break;
                        projectName = ConsoleManager.GetProjectName(projectNames);
                        if (projectName != null)
                            ServerManager.Start(metricName, projectName);
                    }
                }
            }

        }
    }
}