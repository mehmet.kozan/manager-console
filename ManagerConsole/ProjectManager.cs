﻿using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    public class ProjectManager
    {
        public IList<Project> Projects { get; set; }

        private RedmineManager manager { get; set; }
        //todo: limit şimdilik 100 yapıldı ancak test edemedim projeleri eksik çekiyor bu class içerisinde düzenleme yapılıp düzeltilmeli.
        public ProjectManager(RedmineManager Manager)
        {
            manager = Manager;
            var project_parameters = new NameValueCollection { 
                 { "status_id", "*" },
                 { "limit", "100"}
            };

            Projects = manager.GetObjectList<Project>(project_parameters);
        }


        /// <summary>
        /// Proje ismine göre proje idlerini döner.
        /// </summary>
        /// <param name="projectName">proje adı.</param>
        /// <returns>proje idleri</returns>
        public List<string> GetProjectIdByName(string projectName)
        {
            List<string> project_ids = new List<string>();

            if (projectName == "*")
            {
                foreach (var project in Projects)
                {
                    project_ids.Add(project.Id.ToString());
                }
            }
            else
            {
                project_ids.Add(Projects.Where(t => t.Name.ToString() == projectName).SingleOrDefault().Id.ToString());
                return project_ids;
            }
            return project_ids;
        }

        /// <summary>
        /// Proje idsine göre proje ismini döner.
        /// </summary>
        /// <param name="projectId">proje idsi.</param>
        /// <returns>proje ismi(string).</returns>
        public string GetProjectNameById(string projectId)
        {
            string project_name;

            project_name = Projects.Where(t => t.Id.ToString() == projectId).SingleOrDefault().Name.ToString();

            return project_name;
        }








        /// <summary>
        /// Returns a project.
        /// </summary>
        /// <param name="overload"></param>
        /// <returns>one project</returns>
        public Project GetProjectFromUser(int overload)
        {
            Project ret = null;
            int project_index = -1;
            int counter = 0;
            string project_name = "";
            Dictionary<int, string> ProjectDictionary = new Dictionary<int, string>();

            var project_parameters = new NameValueCollection { 
                 { "status_id", "*" }
            };

            var projects = manager.GetObjectList<Project>(project_parameters);

            foreach (var project in projects)
            {
                Console.Write("{0}  ", project.Name);
                ProjectDictionary.Add(counter, project.Name);
                counter++;
            }

            project_name = Util.GetStringFromUser("Enter project name");
            //if (ProjectDictionary.ContainsValue((char.ToUpper(project_name[0]) + project_name.Substring(1))))
            //{
            //    project_index = ProjectDictionary.FirstOrDefault(x => x.Value == (char.ToUpper(project_name[0]) + project_name.Substring(1))).Key;
            //}
            //else
            if (ProjectDictionary.ContainsValue(project_name))
            {
                project_index = ProjectDictionary.FirstOrDefault(x => x.Value == project_name).Key;
            }
            else
            {
                Console.WriteLine("Geçerli bir proje adı girin.");
                ret = GetProjectFromUser(1);
                return ret;
            }

            ret = projects.ElementAt(project_index);

            return ret;
        }

        ///// <summary>
        ///// Returns list of projects.
        ///// </summary>
        ///// <returns>list of projects.</returns>
        //public List<Project> GetProjectFromUser()
        //{
        //    List<Project> ret = new List<Project>();
        //    int project_index = -1;
        //    int counter = 0;
        //    string project_name = "";
        //    Dictionary<int, string> ProjectDictionary = new Dictionary<int, string>();

        //    var project_parameters = new NameValueCollection { 
        //         { "status_id", "*" }
        //    };

        //    var projects = manager.GetObjectList<Project>(project_parameters);

        //    foreach (var project in projects)
        //    {
        //        Console.Write("{0}  ", project.Name);
        //        ProjectDictionary.Add(counter, project.Name);
        //        counter++;
        //    }

        //    project_name = Util.GetStringFromUser("Enter project name");
        //    //if (ProjectDictionary.ContainsValue((char.ToUpper(project_name[0]) + project_name.Substring(1))))
        //    //{
        //    //    project_index = ProjectDictionary.FirstOrDefault(x => x.Value == (char.ToUpper(project_name[0]) + project_name.Substring(1))).Key;
        //    //}
        //    //else
        //    if (project_name != "*")
        //    {
        //        if (ProjectDictionary.ContainsValue(project_name))
        //        {
        //            project_index = ProjectDictionary.FirstOrDefault(x => x.Value == project_name).Key;
        //        }
        //        else
        //        {
        //            Console.WriteLine("Geçerli bir proje adı girin.");
        //            ret = GetProjectFromUser();
        //            return ret;
        //        }

        //        ret.Add(projects.ElementAt(project_index));
        //    }
        //    else if (project_name == "*")
        //    {
        //        foreach (var project in projects)
        //        {
        //            projects.Add(project);
        //        }
        //    }
        //    return ret;
        //}
    }
}
