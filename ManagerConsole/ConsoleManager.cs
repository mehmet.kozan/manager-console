﻿using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    // static 
    public static class ConsoleManager
    {
        //public static List<Tuple<int, string>> Projects { get; set; }
        //public static List<Tuple<int, string>> Metrics { get; set; }
        public static List<string> MetricNameList { get; set; }


        public static RedmineUserAccount GetRedmineAccountInfo()
        {
            RedmineUserAccount redmineUserAccount = new RedmineUserAccount();
            //ConfigManager configManager = new ConfigManager();
            Console.WriteLine("");
            Console.WriteLine("[1] Varsayılan değerler.(Config) ");
            Console.WriteLine("[2] Yeni değer girişi.(Console) ");
            Console.WriteLine("[3] Çıkış. ");

            while (true)
            {
                int userSelection = Util.GetIntFromUser(1, 3);

                switch (userSelection)
                {
                    case 3:
                        return null;
                    case 1:
                        return ConfigManager.GetRedmineAccountInfo();
                    case 2:
                        redmineUserAccount.RedmineUrl = Util.GetStringFromUser("Redmine Url");

                        if (!(redmineUserAccount.RedmineUrl.ToLower().StartsWith("http://") || redmineUserAccount.RedmineUrl.ToLower().StartsWith("https://")))
                        {
                            redmineUserAccount.RedmineUrl = "http://" + redmineUserAccount.RedmineUrl;
                        }
                        else if (redmineUserAccount.RedmineUrl == null)
                        {
                            Console.WriteLine("Lütfen host adresi giriniz.");
                            break;
                        }

                        redmineUserAccount.UserName = Util.GetStringFromUser("Kullanıcı adı");

                        redmineUserAccount.Password = Util.GetStringFromUser("Şifre");

                        return redmineUserAccount;
                }
            }
        }


        /// <summary>
        /// Gets metric name from user via console.
        /// </summary>
        /// <param name="MetricManager">MetricManager</param>
        /// <returns>metric name(string).</returns>
        public static string GetMetricName(MetricManager MetricManager)
        {
            string metricName;

            while (true)
            {
                Console.Write("\n");

                foreach (var item in MetricManager.MetricDictionary.Keys)
                {
                    Console.Write(string.Format(" {0}", item));
                }

                Console.WriteLine("");

                while (true)
                {
                    metricName = Util.GetStringFromUser("Please enter any metric name.\n To exit, \"Exit\". \n To execute all metrics, \"*\".");

                    if (metricName.ToLower() == "exit")
                    {
                        break;
                    }
                    else if (MetricManager.MetricDictionary.ContainsKey(metricName))
                    {
                        return metricName;
                    }
                    else if (MetricManager.MetricDictionary.ContainsKey(metricName.ToUpper()))
                    {
                        return metricName;
                    }
                    else if (metricName == "*")
                    {
                        return metricName;
                    }
                    else
                    {
                        Console.Error.WriteLine("Please enter any valid metric name or \"Exit\".");
                    }
                }
                if (metricName.ToLower() == "exit")
                {
                    Console.WriteLine("");
                    GetRedmineAccountInfo();
                    return null;
                }
                //---------------------
            }
        }



        /// <summary>
        /// Gets metric name from user via console.
        /// </summary>
        /// <param name="MetricNameList">Metric Name List string.</param>
        /// <returns>metric name(string).</returns>
        public static string GetMetricName(List<string> MetricNameTempList)
        {
            ConsoleManager.MetricNameList = MetricNameTempList;
            string metricName;

            while (true)
            {
                Console.Write("\n");

                foreach (var item in MetricNameList)
                {
                    Console.Write(string.Format(" {0}", item));
                }

                Console.WriteLine("");

                while (true)
                {
                    metricName = Util.GetStringFromUser("Please enter any metric name.\n To exit, \"Exit\". \n To execute all metrics, \"*\".");

                    if (metricName.ToLower() == "exit")
                    {
                        return null;
                    }
                    else if (MetricNameList.Contains(metricName))
                    {
                        return metricName;
                    }
                    else if (MetricNameList.Contains(metricName.ToUpper()))
                    {
                        return metricName;
                    }
                    else if (metricName == "*")
                    {
                        return metricName;
                    }
                    else
                    {
                        Console.Error.WriteLine("Please enter any valid metric name or \"Exit\".");
                    }
                }
                if (metricName.ToLower() == "exit")
                {
                    Console.WriteLine("");
                    Program.Main(Program.argumans);
                    return null;
                }
            }
        }


        /// <summary>
        /// Gets project name from user.
        /// </summary>
        /// <param name="ProjectList">Project Name List String.</param>
        /// <returns>project name (string).</returns>
        public static string GetProjectName(List<string> ProjectNameList)
        {//todo: exit girince getmetricname fonksiyonuna geri dönmesi lazım.
            string projectName;
            int projectIndex;
            int index = 1;

            Console.WriteLine("");

            ProjectNameList.Add("Exit");
            foreach (var project in ProjectNameList)
            {
                Console.Write(" [{0}],{1}", index++, project);
            }

            while (true)
            {
                projectIndex = Util.GetIntFromUser("Please enter any project name.\n To execute all projects, \"*\".", 1, ProjectNameList.Count);
                projectName = ProjectNameList.ElementAt(projectIndex - 1);
                if (projectName.ToLower() == "exit")
                {
                    Console.WriteLine("");
                    GetMetricName(ConsoleManager.MetricNameList);
                    return null;
                }
                else if (ProjectNameList.Contains(projectName))
                {
                    return projectName;
                }
                else if (ProjectNameList.Contains(projectName.ToUpper()))
                {
                    return projectName;
                }
                else if (projectName == "*")
                {
                    return projectName;
                }
                else
                {
                    Console.Error.WriteLine("Please enter any valid project name or \"Exit\".");
                }
            }

        }

        /// <summary>
        /// Gets project name from user.
        /// </summary>
        /// <param name="RedmineManager">Redmine manager</param>
        /// <returns>project name (string).</returns>
        public static string GetProjectName(RedmineManager RedmineManager)
        {
            string projectName;

            List<string> ProjectList = new List<string>();

            var project_parameters = new NameValueCollection
            { 
                 { "status_id", "*" }
            };

            var projects = RedmineManager.GetObjectList<Project>(project_parameters);

            Console.WriteLine("");


            foreach (var project in projects)
            {
                Console.Write(" {0}", project.Name);
                ProjectList.Add(project.Name);
            }

            while (true)
            {
                projectName = Util.GetStringFromUser("Please enter any project name.\n To exit, \"Exit\". \n To execute all projects, \"*\".");
                if (projectName == "Exit")
                {
                    Environment.Exit(0);
                }
                else if (ProjectList.Contains(projectName))
                {
                    return projectName;
                }
                else if (ProjectList.Contains(projectName.ToUpper()))
                {
                    return projectName;
                }
                else if (projectName == "*")
                {
                    return projectName;
                }
                else
                {
                    Console.Error.WriteLine("Please enter any valid project name or \"Exit\".");
                }
            }
        }



    }
}
