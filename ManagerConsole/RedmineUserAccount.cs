﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    public class RedmineUserAccount
    {
        public string RedmineUrl { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
