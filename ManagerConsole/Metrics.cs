﻿using ManagerConsole.MetricResults;
using Redmine.Net.Api.Types;
using Redmine.Net.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerConsole
{
    public partial class MetricManager
    {
        //// eforun toplam sayısı dönmediği için her issuenunkini tek tek toplamak gerekiyor. Bu performans sorununa sebep oluyor.
        ///// <summary>
        ///// Proje yönetimi eforu, kalite eforu ve konfigürasyon efor toplamının, toplam efora oranını hesaplar.
        ///// </summary>
        //public void M004()
        //{
        //    //List<string> proj_id = new List<string>();

        //    //proj_id = MetrikProjeId();
        //    //Console.WriteLine(string.Format("\nProje Yönetimi Efor Metriği"));

        //    //foreach (var project_id in proj_id)
        //    //{
        //    //    M004 m004 = new M004();

        //    //    DateTime now = DateTime.Now;
        //    //    m004.Date = new DateTime(now.Year, now.Month, 1);

        //    //    double proje_yonetimi_eforu = IssueManager.GetTimeEntryCount(project_id, "Proje Yönetimi");
        //    //    double kalite_eforu = IssueManager.GetTimeEntryCount(project_id, "Kalite");
        //    //    double konfigurasyon_eforu = IssueManager.GetTimeEntryCount(project_id, "Konfigürasyon");
        //    //    double toplam_efor = IssueManager.GetTimeEntryCount(project_id, "*");

        //    //    m004.KaliteEforu = kalite_eforu;
        //    //    m004.ProjeYonetimEforu = proje_yonetimi_eforu;
        //    //    m004.KonfigurasyonEforu = konfigurasyon_eforu;
        //    //    m004.ToplamEfor = toplam_efor;

        //    //    Console.WriteLine(m004.ToString());
        //    //}
        //}


        ///// <summary>
        /////          Gerçekleşen risklerin tüm risklere oranını hesaplar.
        ///// </summary>
        //public void M005()
        //{
        //    //List<string> proj_id = new List<string>();
        //    //ProjectManager proj = new ProjectManager(redmineManager);

        //    //if (Argumanlar != null)
        //    //{
        //    //    if (Argumanlar[1] == "*")
        //    //    {
        //    //        foreach (var p in proj.Projects)
        //    //        {
        //    //        }
        //    //    }
        //    //    else
        //    //    {
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //}

        //    //foreach (var project_id in proj_id)
        //    //{
        //    //} 

        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    Console.WriteLine(string.Format("\nRisk Durum Metriği"));

        //    M005 m005 = new M005();

        //    DateTime now = DateTime.Now;

        //    int gerceklesen_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Gerçekleşti");
        //    int tum_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "*");

        //    m005.Date = new DateTime(now.Year, now.Month, 1);

        //    m005.GerceklesenRiskler = gerceklesen_riskler;
        //    m005.TumRiskler = tum_riskler;

        //    Console.WriteLine(m005.ToString());  
        //}


        ///// <summary>
        ///// Açık Kırmızı Risklerin Açık Tüm Risklere oranını bulur(cf_42).
        ///// </summary>
        //public void M006()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    Console.WriteLine(string.Format("\nRisk Renk Metriği"));

        //    M006 m006 = new M006();

        //    DateTime now = DateTime.Now;

        //    int yesil_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "Yeşil", "");
        //    int sari_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "Sarı", "");
        //    int kirmizi_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "Kırmızı", "");
        //    int tum_riskler = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Risk", "Riskin Rengi", "*", "");


        //    m006.Date = new DateTime(now.Year, now.Month, 1);

        //    m006.YesilRiskler = yesil_riskler;
        //    m006.SariRiskler = sari_riskler;
        //    m006.KirmiziRiskler = kirmizi_riskler;
        //    m006.TumRiskler = tum_riskler;

        //    Console.WriteLine(m006.ToString());    
        //}


        ///// <summary>
        ///// Gözden geçirme eforunun toplam efora oranını hesaplar.
        ///// </summary>
        //public void M010()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    Console.WriteLine(string.Format("\nGözden Geçirme Efor Metriği"));

        //    var project_id = project.Id.ToString();

        //    M010 m010 = new M010();

        //    DateTime now = DateTime.Now;

        //    double gozden_gecirme_eforu = IssueManager.GetTimeEntryCount(project_id, "Gözden Geçirme");
        //    double toplam_efor = IssueManager.GetTimeEntryCount(project_id, "*");

        //    m010.Date = new DateTime(now.Year, now.Month, 1);

        //    m010.GozdenGecirmeEforu = gozden_gecirme_eforu;
        //    m010.ToplamEfor = toplam_efor;

        //    Console.WriteLine(m010.ToString());    
        //}


        ///// <summary>
        ///// Yeni ve devam ediyor durumundaki döflerin toplamını hesaplar.
        ///// </summary>
        //public void M013()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    Console.WriteLine(string.Format("\nDÖF Metriği"));

        //    M013 m013 = new M013();

        //    DateTime now = DateTime.Now;

        //    int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Yeni");
        //    int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Devam Ediyor");
        //    int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Çözüldü");
        //    int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Kapatıldı");
        //    int reddedildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "Reddedildi");
        //    int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Düzeltici/Önleyici Faaliyet", "İptal");


        //    m013.Date = new DateTime(now.Year, now.Month, 1);

        //    m013.Yeni = yeni;
        //    m013.DevamEdiyor = devam_ediyor;
        //    m013.Cozuldu = cozuldu;
        //    m013.Kapatildi = kapatildi;
        //    m013.Reddedildi = reddedildi;
        //    m013.Iptal = iptal;


        //    Console.WriteLine(m013);    
        //}


        ///// <summary>
        ///// Gereklilik trackerı içinde başlanmamış,devam eden, kodlanmış,biten statulerdeki issue sayılarını hesaplar.
        ///// </summary>
        //public void M014()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M014 m014 = new M014();

        //    Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Yeni");
        //    int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Devam Ediyor");
        //    int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Daha çok bilgi gerekiyor");
        //    int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Testten Geçemedi");
        //    int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü");
        //    int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)");
        //    int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
        //    int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
        //    int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

        //    int baslanmamis = yeni;
        //    int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
        //    int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    int biten = kapatildi + dogrulandi + iptal;



        //    m014.Baslanmamis = baslanmamis;
        //    m014.DevamEden = devam_eden;
        //    m014.Kodlanmis = kodlanmis;
        //    m014.Biten = biten;

        //    Console.WriteLine(m014);
        //}


        ////(Gereksinim Tamamlanma Metriği / (Gerçekleşen Efor / Planlanan Toplam Efor)) spent time returns null
        //public void M015()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M015 m015 = new M015();

        //    double planlanan_efor;
        //    Console.Write("Planlanan eforu girin:");
        //    planlanan_efor = double.Parse(Console.ReadLine());

        //    Console.WriteLine(string.Format("\nGereksinim Tamamlanma Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int toplam = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "*");
        //    int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
        //    int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
        //    int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

        //    double gerceklesen_efor = IssueManager.GetTimeEntryCount(project_id, "*");

        //    int biten = kapatildi + dogrulandi + iptal;

        //    m015.Date = new DateTime(now.Year, now.Month, 1);

        //    m015.Toplam = toplam;
        //    m015.Biten = biten;

        //    m015.GerceklesenEfor = gerceklesen_efor;
        //    m015.PlanlananEfor = planlanan_efor;

        //    Console.WriteLine(m015);
        //}


        ///// <summary>
        ///// Efor(Analiz + Tasarım + Geliştirme) / Gereksinim((Devam Eden * 0.5) + Kodlanmış + Biten) oranını hesaplar. (Gereksinim Eforu Metriği (saat/gereksinim) < ??)?.
        ///// </summary>
        //public void M016()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M016 m016 = new M016();

        //    Console.WriteLine(string.Format("\nGereksinim Eforu Metriği: "));

        //    DateTime now = DateTime.Now;

        //    double devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Devam Ediyor");
        //    double daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Daha çok bilgi gerekiyor");
        //    double testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Testten Geçemedi");
        //    double cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü");
        //    double tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)");
        //    double kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı");
        //    double dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı");
        //    double iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal");

        //    double devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
        //    double kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    double biten = kapatildi + dogrulandi + iptal;

        //    double analiz_eforu = IssueManager.GetTimeEntryCount(project_id, "Analiz");
        //    double tasarim_eforu = IssueManager.GetTimeEntryCount(project_id, "Tasarım");
        //    double gelistirme_eforu = IssueManager.GetTimeEntryCount(project_id, "Geliştirme");


        //    m016.Date = new DateTime(now.Year, now.Month, 1);

        //    m016.AnalizEforu = analiz_eforu;
        //    m016.TasarimEforu = tasarim_eforu;
        //    m016.GelistirmeEforu = gelistirme_eforu;

        //    m016.DevamEden = devam_eden;
        //    m016.Kodlanmis = kodlanmis;
        //    m016.Biten = biten;

        //    Console.WriteLine(m016);    
        //}


        ///// <summary>
        ///// (Durum=İptal) + (Kaynağı=Ek Talep)(cf_31) / Toplam Gereksinim  oranını hesaplar.
        ///// </summary>
        //public void M017()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));


        //    var project_id = project.Id.ToString();

        //    M017 m017 = new M017();

        //    Console.WriteLine(string.Format("\nGereksinimlerin Değişkenliği Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int ek_talep_iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kaynağı", "Ek Talep", "İptal");
        //    int toplam = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "*");

        //    m017.Date = new DateTime(now.Year, now.Month, 1);

        //    m017.EkTalepIptal = ek_talep_iptal;
        //    m017.Toplam = toplam;

        //    Console.WriteLine(m017);
        //}

        ////roadmap = versiyon.
        ///// <summary>
        ///// RoadMap Gerçekleşme Metriği.
        ///// </summary>
        //public void M018()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M018 m018 = new M018();

        //    Console.WriteLine(string.Format("\nRoadMap Gerçekleşme Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int gereksinim = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "Gereksinim", "*");
        //    int hata = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "Hata", "*");
        //    int islem_maddesi = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "İşlem Maddesi", "*");
        //    int gorev = IssueManager.GetIssueCountByTrackerStatusAndVersion(project_id, "Görev", "*");

        //    m018.Date = new DateTime(now.Year, now.Month, 1);

        //    m018.Gereksinim = gereksinim;
        //    m018.Hata = hata;
        //    m018.IslemMaddesi = islem_maddesi;
        //    m018.Gorev = gorev;

        //    Console.WriteLine(m018);
        //}


        ///// <summary>
        ///// Kullanım durumu metriği.
        ///// </summary>
        //public void M019()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M019 m019 = new M019();

        //    Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int ilk_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "İlk KD");
        //    int degisen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Değişen KD");
        //    int eklenen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Eklenen KD");
        //    int cikarilan_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Çıkarılan KD");
        //    int silinen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Silinen KD");

        //    m019.Date = new DateTime(now.Year, now.Month, 1);

        //    m019.Ilk = ilk_kd;
        //    m019.Degisen = degisen_kd;
        //    m019.Eklenen = eklenen_kd;
        //    m019.Cıkartılan = cikarilan_kd;
        //    m019.Silinen = silinen_kd;

        //    Console.WriteLine(m019);
        //}

        ///// <summary>
        ///// Kullanım Durumunun Değişkenliği Metriği.
        ///// </summary>
        //public void M020()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M020 m020 = new M020();

        //    Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int ilk_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "İlk KD");
        //    int degisen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Değişen KD");
        //    int eklenen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Eklenen KD");
        //    int cikarilan_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Çıkarılan KD");
        //    int silinen_kd = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Kullanım Durumları", "Silinen KD");

        //    m020.Date = new DateTime(now.Year, now.Month, 1);

        //    m020.Ilk = ilk_kd;
        //    m020.Degisen = degisen_kd;
        //    m020.Eklenen = eklenen_kd;
        //    m020.Cikartilan = cikarilan_kd;
        //    m020.Silinen = silinen_kd;

        //    Console.WriteLine(m020);            
        //}


        ///// <summary>
        ///// Toplam MDK Sayısını hesaplar.
        ///// </summary>
        //public void M021()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M021 m021 = new M021();

        //    Console.WriteLine(string.Format("\nGereksinim Dağılımı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int acik_mdk = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Mühendislik Değişiklik Kaydı", "");
        //    int kapali_mdk = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Mühendislik Değişiklik Kaydı", "Kapatıldı");

        //    m021.Date = new DateTime(now.Year, now.Month, 1);

        //    m021.AcikMDK = acik_mdk;
        //    m021.KapaliMDK = kapali_mdk;

        //    Console.WriteLine(m021);                
        //}


        ///// <summary>
        ///// (Toplam Hata Sayısı / Kodlanmış + Biten Gereksinim Sayısı)  oranını hesaplar. (/gereksinim?)
        ///// </summary>
        //public void M022()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M022 m022 = new M022();

        //    Console.WriteLine(string.Format("\nHata Dağılımı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Yeni");
        //    int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Devam Ediyor");
        //    int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Daha çok bilgi gerekiyor");
        //    int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Testten Geçemedi");
        //    int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Çözüldü");
        //    int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Tamamlandı (Versiyona hazır)");
        //    int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Kapatıldı");
        //    int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Doğrulandı");
        //    int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "İptal");

        //    //int cozuldu_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü", start);
        //    //int tamamlandi_versiyona_hazir_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)", start);
        //    //int kapatildi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı", start);
        //    //int dogrulandi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı", start);
        //    //int iptal_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal", start);

        //    int baslanmamis = yeni;
        //    int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
        //    int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    int biten = kapatildi + dogrulandi + iptal;

        //    //int kodlanmis_gereksinim = cozuldu_gereksinim + tamamlandi_versiyona_hazir_gereksinim;
        //    //int biten_gereksinim = kapatildi_gereksinim + dogrulandi_gereksinim + iptal_gereksinim;


        //    m022.Date = new DateTime(now.Year, now.Month, 1);

        //    m022.Baslanmamis = baslanmamis;
        //    m022.DevamEden = devam_eden;
        //    m022.Kodlanmis = kodlanmis;
        //    m022.Biten = biten;

        //    //m022.KodlanmisGereksinim = kodlanmis_gereksinim;
        //    //m022.BitenGereksinim = biten_gereksinim;

        //    Console.WriteLine(m022);    
        //}


        ///// <summary>
        ///// (Hata(Önem=1) / Gereksinim( Kodlanmış + Biten)) < 0.2 oranını hesaplar. (gereksinim?)
        ///// </summary>
        //public void M023()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M023 m023 = new M023();

        //    Console.WriteLine(string.Format("\nAçık Hata Önem Dağılımı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int acik_hata_onem_1 = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "");

        //    int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Yeni");

        //    int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Devam Ediyor");
        //    int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Daha çok bilgi gerekiyor");
        //    int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Testten Geçemedi");
        //    int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Çözüldü");
        //    int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Tamamlandı (Versiyona hazır)");

        //    int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;

        //    acik_hata_onem_1 = kodlanmis + devam_eden + yeni;

        //    cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Çözüldü");
        //    tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Tamamlandı (Versiyona hazır)");
        //    int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Kapatıldı");
        //    int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Doğrulandı");
        //    int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "İptal");

        //    //int cozuldu_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Çözüldü", start);
        //    //int tamamlandi_versiyona_hazir_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Tamamlandı (Versiyona hazır)", start);
        //    //int kapatildi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Kapatıldı", start);
        //    //int dogrulandi_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "Doğrulandı", start);
        //    //int iptal_gereksinim = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Gereksinim", "İptal", start);


        //    kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    int biten = kapatildi + dogrulandi + iptal;

        //    //int kodlanmis_gereksinim = cozuldu_gereksinim + tamamlandi_versiyona_hazir_gereksinim;
        //    //int biten_gereksinim = kapatildi_gereksinim + dogrulandi_gereksinim + iptal_gereksinim;


        //    m023.Date = new DateTime(now.Year, now.Month, 1);

        //    m023.AcikHataOnem1 = acik_hata_onem_1;
        //    m023.Kodlanmis = kodlanmis;
        //    m023.Biten = biten;

        //    //m022.KodlanmisGereksinim = kodlanmis_gereksinim;
        //    //m022.BitenGereksinim = biten_gereksinim;

        //    Console.WriteLine(m023);    
        //}


        ///// <summary>
        ///// #(Yaş(Hata(Önem=1, 2))>30 gün) = 0 eşitliğini sorgular.
        ///// </summary>
        //public void M024()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M024 m024 = new M024();

        //    Console.WriteLine(string.Format("\nAçık Hata Yaşı Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Yeni");
        //    int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Devam Ediyor");
        //    int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Daha çok bilgi gerekiyor");
        //    int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Testten Geçemedi");
        //    int cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Çözüldü");
        //    int tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "1 (En Önemli)", "Tamamlandı (Versiyona hazır)");

        //    int kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;

        //    int acik_hata_onem_1 = kodlanmis + devam_eden + yeni;

        //    yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Yeni");
        //    devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Devam Ediyor");
        //    daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Daha çok bilgi gerekiyor");
        //    testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Testten Geçemedi");
        //    cozuldu = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Çözüldü");
        //    tamamlandi_versiyona_hazir = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Önemi", "2", "Tamamlandı (Versiyona hazır)");

        //    kodlanmis = cozuldu + tamamlandi_versiyona_hazir;
        //    devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;

        //    int acik_hata_onem_2 = kodlanmis + devam_eden + yeni;

        //    m024.Date = new DateTime(now.Year, now.Month, 1);

        //    m024.AcikHataOnem1 = acik_hata_onem_1;
        //    m024.AcikHataOnem2 = acik_hata_onem_2;

        //    Console.WriteLine(m024);      
        //}


        ///// <summary>
        ///// * Açık Hata-> Başlanmamış+Devam Eden-Hata Değil ve * Kapalı Hata + Hata Değil-> Biten+Hata Değil değerlerini hesaplar.
        ///// </summary>
        //public void M025()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();

        //    M025 m025 = new M025();

        //    Console.WriteLine(string.Format("\nHata Durumu Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int yeni = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Yeni");
        //    int devam_ediyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Devam Ediyor");
        //    int daha_cok_bilgi_gerekiyor = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Daha çok bilgi gerekiyor");
        //    int testten_geçemedi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Testten Geçemedi");
        //    int kapatildi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Kapatıldı");
        //    int dogrulandi = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Doğrulandı");
        //    int iptal = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "İptal");

        //    int baslanmamis = yeni;
        //    int devam_eden = devam_ediyor + daha_cok_bilgi_gerekiyor + testten_geçemedi;
        //    int biten = kapatildi + dogrulandi + iptal;

        //    int hata_degil = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Hata Değil");

        //    m025.Date = new DateTime(now.Year, now.Month, 1);

        //    m025.Baslanmamis = baslanmamis;
        //    m025.DevamEden = devam_eden;
        //    m025.Biten = biten;
        //    m025.HataDegil = hata_degil;

        //    Console.WriteLine(m025);    
        //}



        ///// <summary>
        ///// Dış Kaynaklı Hata Sayısı < İç Kaynaklı Hata Sayısı (hata kaynağı=Bulgu Kaynağı?)
        ///// </summary>
        //public void M026()
        //{
        //    Project project = ProjectManager.GetProjectFromUser(1);

        //    Console.WriteLine(string.Format("Selected Project Name: {0}", project.Name));

        //    var project_id = project.Id.ToString();                      

        //    M026 m026 = new M026();

        //    Console.WriteLine(string.Format("\nİç/Dış Kaynaklı Hata Metriği: "));

        //    DateTime now = DateTime.Now;

        //    int dis_kaynakli_hata = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Bulgu Kaynağı", "Dış Kaynaklı", "*");
        //    int ic_kaynakli_hata = IssueManager.GetIssueCountByTrackerAndStatus(project_id, "Hata", "Bulgu Kaynağı", "İç Kaynaklı", "*");

        //    m026.Date = new DateTime(now.Year, now.Month, 1);

        //    m026.DisKaynakliHataSayisi = dis_kaynakli_hata;
        //    m026.IcKaynakliHataSayisi = ic_kaynakli_hata;

        //    Console.WriteLine(m026);                            
        //}
    }
}
