﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// Efor(Analiz + Tasarım + Geliştirme) / Gereksinim((Devam Eden * 0.5) + Kodlanmış + Biten) oranını hesaplar. (Gereksinim Eforu Metriği (saat/gereksinim) < ??)?.
    /// </summary>
    /// 
    [Serializable]
    public class M016 : MetricResult<M016>
    {
        [XmlElement]
        public double AnalizEforu { get; set; }
        [XmlElement]
        public double TasarimEforu { get; set; }
        [XmlElement]
        public double GelistirmeEforu { get; set; }
        [XmlElement]
        public double DevamEden { get; set; }
        [XmlElement]
        public double Kodlanmis { get; set; }
        [XmlElement]
        public double Biten { get; set; }
        [XmlElement]
        public double Toplam { get; set; }

        [XmlElement]
        public double Result { get; set; }
        [XmlElement]
        public double Percentage { get; set; }

        public bool CalculateResult()
        {
            bool t;

            Result = (AnalizEforu + TasarimEforu + GelistirmeEforu) / ((DevamEden * 0.5) + Kodlanmis + Biten);
            Toplam = DevamEden + Kodlanmis + Biten;

            if (true)
            {
                t = true;
            }
            //else if (double.IsNaN(Result))
            //{
            //    t = true;
            //}
            //else
            //{
            //    t = false;
            //}

            return t;
        }

        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();

            bool t = CalculateResult();

            if (Toplam != 0)
            {

                sb.Append(string.Format("Sonuc( {0}/{1} ): Gereksinim Eforu Metriği:{2:f2}", ExecuteDate.Month, ExecuteDate.Year, Result));
                sb.Append(string.Format(" AnalizEforu:{0} saat, TasarimEforu:{1} saat, GelistirmeEforu:{2} saat,", AnalizEforu, TasarimEforu, GelistirmeEforu));
                sb.Append(string.Format(" DevamEden:{0} adet, Kodlanmis:{1} adet, Biten:{2} adet", DevamEden, Kodlanmis, Biten));

                if (!t)
                {
                    sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
                }
            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ): Gereksinim Eforu Metriği:{2:f2}", ExecuteDate.Month, ExecuteDate.Year, Result));
                sb.Append(string.Format(" AnalizEforu:{0} saat, TasarimEforu:{1} saat, GelistirmeEforu:{2} saat", AnalizEforu, TasarimEforu, GelistirmeEforu));
                sb.Append(string.Format(" Gereksinim bulunamadı. "));
            }

            
            return sb.ToString();

        }

    }
}
