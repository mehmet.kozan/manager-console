﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    [Serializable]
    public class M005 : MetricResult<M005>
    {
        [XmlElement]
        public double GerceklesenRiskler { get; set; }
        [XmlElement]
        public double TumRiskler { get; set; }
        [XmlElement]
        public double Result { get; set; }
        [XmlElement]
        public int Percentage { get; set; }


        public bool CalculateResult()
        {
            bool t;

            Result = GerceklesenRiskler / TumRiskler;
            Percentage = (int)Math.Round(Result * 100);

            if (Result<(0.2))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = true;
            }
            else
            {
                t = false;
            }

            return t;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (TumRiskler != 0)
            {
                bool t = CalculateResult();

                sb.Append(string.Format("Sonuc( {0}/{1} ): ", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format("{0:f2} ",Result));
                sb.Append(string.Format("GerceklesenRiskler:{0} adet, TumRiskler:{1} adet,", GerceklesenRiskler, TumRiskler));                
                sb.Append(string.Format(" Risk Durum Metriği: %{0} ", Percentage));

                if (!t)
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli değil."));
                }
            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format(" Risk bulunamadı. "));
            }
            

            return sb.ToString();

        }

    }
}
