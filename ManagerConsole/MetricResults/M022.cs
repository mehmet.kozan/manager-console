﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// (Toplam Hata Sayısı / Kodlanmış + Biten Gereksinim Sayısı)  oranını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M022 : MetricResult<M022>
    {
        [XmlElement]
        public double Baslanmamis { get; set; }
        [XmlElement]
        public double DevamEden { get; set; }
        [XmlElement]
        public double Kodlanmis { get; set; }
        [XmlElement]
        public double Biten { get; set; }
        [XmlElement]
        public double KodlanmisGereksinim { get; set; }
        [XmlElement]
        public double BitenGereksinim { get; set; }
        [XmlElement]
        public double Toplam { get; set; }

        //public double KodlanmisGereksinim { get; set; }
        //public double BitenGereksinim { get; set; }
        //public double ToplamGereksinim { get; set; }

        [XmlElement]
        public double Result { get; set; }

        public bool CalculateResult()
        {
            bool t;

            Toplam = Baslanmamis + DevamEden + Kodlanmis + Biten;
            //ToplamGereksinim = KodlanmisGereksinim + BitenGereksinim;
            Result = Toplam / (KodlanmisGereksinim + BitenGereksinim);

            if (Result < (1))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = true;
            }
            else
            {
                t = false;
            }
            return t;
        }

        
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): Baslanmamis:{2} adet, DevamEden:{3} adet,", ExecuteDate.Month, ExecuteDate.Year, Baslanmamis, DevamEden));
            sb.Append(string.Format(" Kodlanmis:{0} adet, Biten:{1} adet,", Kodlanmis, Biten));
            sb.Append(string.Format(" KodlanmisGereksinim:{0} adet, BitenGereksinim:{1} adet,", KodlanmisGereksinim, BitenGereksinim));
            sb.Append(string.Format(" Toplam:{0} adet, Hata Dağılımı Metriği:{1:f2}", Toplam, Result));

            if (!t)
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            }
            else
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            }


            return sb.ToString();                            
        }
    }
}
