﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// (Toplam Hata Sayısı / Kodlanmış + Biten Gereksinim Sayısı)  oranını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M025 : MetricResult<M025>
    {
        [XmlElement]
        public double Baslanmamis { get; set; }
        [XmlElement]
        public double DevamEden { get; set; }
        [XmlElement]
        public double HataDegil { get; set; }
        [XmlElement]
        public double Biten { get; set; }
        [XmlElement]
        public double AcikHata { get; set; }
        [XmlElement]
        public double KapaliHataVeHataDegil { get; set; }

        //public double KodlanmisGereksinim { get; set; }
        //public double BitenGereksinim { get; set; }
        //public double ToplamGereksinim { get; set; }

        [XmlElement]
        public double Result { get; set; }

        public bool CalculateResult()
        {
            bool t = true;

            AcikHata = Baslanmamis + DevamEden - HataDegil;
            KapaliHataVeHataDegil = Biten + HataDegil;

            //if (Result < (1))
            //{
            //    t = true;
            //}
            //else
            //{
            //    t = false;
            //}
            return t;
        }
        
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): Başlanmamış:{2} adet, Devam Eden:{3} adet, Biten:{4} adet", ExecuteDate.Month, ExecuteDate.Year, Baslanmamis, DevamEden , Biten));
            sb.Append(string.Format(" Hata Değil:{0} adet, Acik Hata:{1} adet (Kapali Hata + Hata Degil):{2} adet", HataDegil, AcikHata, KapaliHataVeHataDegil));
            //if (!t)
            //{
            //    sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            //}
            //else
            //{
            //    sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            //}


            return sb.ToString();
        }
    }
}
