﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    [Serializable]
    public class M004 : MetricResult<M004>
    {
        [XmlElement]
        public double ProjeYonetimEforu{get;set;}
        [XmlElement]
        public double KaliteEforu { get; set; }
        [XmlElement]
        public double KonfigurasyonEforu { get; set; }
        [XmlElement]
        public double ToplamEfor { get; set; }
        [XmlElement]
        public double Result { get; set; }
        [XmlElement]
        public int Percentage { get; set; }


        public bool CalculateResult() 
        {
            bool t;
            Result = (ProjeYonetimEforu + KaliteEforu + KonfigurasyonEforu) / ToplamEfor;
            Percentage = (int)Math.Round(Result * 100);

            if (Result<(0.18) && Result > (0.05))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = false;
            }
            else
            {
                t = false;
            }
 
            return t;
        }

        public override string ToString()
        {

            bool t = CalculateResult();
            StringBuilder sb = new StringBuilder();

            if (ToplamEfor != 0)
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format("{0:f2} ", Result));
                sb.Append(string.Format("ProjeYonetimEforu:{0} saat, KaliteEforu:{1} saat, KonfigurasyonEforu:{2} saat, ToplamEfor:{3} saat,", ProjeYonetimEforu, KaliteEforu, KonfigurasyonEforu, ToplamEfor));
                sb.Append(string.Format(" PY Efor Metriği: %{0} ", Percentage));

                if (!t)
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli değil."));
                }

            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format(" Efor bulunamadı. "));
            }

            return sb.ToString();

        }
        
    }
}
