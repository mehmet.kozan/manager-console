﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// Gereklilik trackerı içinde başlanmamış,devam eden, kodlanmış,biten statulerdeki issue sayılarını hesaplar.
    /// </summary>
    [Serializable]
    public class M015 : MetricResult<M015>
    {
        [XmlElement]
        public double Toplam { get; set; }
        [XmlElement]
        public double Biten { get; set; }
        [XmlElement]
        public double GerceklesenEfor { get; set; }
        [XmlElement]
        public double PlanlananEfor { get; set; }


        [XmlElement]
        public double Result { get; set; }
        [XmlElement]
        public double GereksinimTamamlanmaMetrigi { get; set; }

        public bool CalculateResult()
        {
            bool t;

            GereksinimTamamlanmaMetrigi = Biten / Toplam;
            Result = GereksinimTamamlanmaMetrigi / (GerceklesenEfor / PlanlananEfor);

            if (Result > (0.9))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = false;
            }
            else
            {
                t = false;
            }

            return t;
        }

        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            if (Toplam != 0)
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ): Biten:{2} adet, Toplam:{3} adet,", ExecuteDate.Month, ExecuteDate.Year, Biten, Toplam));
                sb.Append(string.Format(" Gerceklesen Efor:{0} saat, Planlanan Efor:{1} saat,", GerceklesenEfor, PlanlananEfor));
                sb.Append(string.Format(" GereksinimTamamlanmaMetrigi:{0:f2} Result:{1:f2}" , GereksinimTamamlanmaMetrigi, Result));
                if (!t)
                {
                    sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
                }
            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):  Gerceklesen Efor:{2} saat, Planlanan Efor:{3} saat,", ExecuteDate.Month, ExecuteDate.Year, GerceklesenEfor, PlanlananEfor)); 
                sb.Append(string.Format("GereksinimTamamlanmaMetrigi:{0:f2} Result:{1:f2}", GereksinimTamamlanmaMetrigi, Result));
                sb.Append(string.Format(" Gereksinim bulunamadı. "));
            }
            return sb.ToString();

        }

    }
}
