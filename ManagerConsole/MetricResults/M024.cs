﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// #(Yaş(Hata(Önem=1, 2))>30 gün) = 0 eşitliğini sorgular.
    /// </summary>
    /// 
    [Serializable]
    public class M024 : MetricResult<M024>
    {
        [XmlElement]
        public int AcikHataOnem1 { get; set; }
        [XmlElement]
        public int AcikHataOnem2 { get; set; }

        //public double KodlanmisGereksinim { get; set; }
        //public double BitenGereksinim { get; set; }
        //public double ToplamGereksinim { get; set; }

        [XmlElement]
        public int Result { get; set; }

        public bool CalculateResult()
        {
            bool t;

            Result = AcikHataOnem1 + AcikHataOnem2;
            //ToplamGereksinim = KodlanmisGereksinim + BitenGereksinim;


            if (Result == 0)
            {
                t = true;
            }
            else
            {
                t = false;
            }
            return t;
        }

        
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): Acik Hata (Onem=1)(Yaş>30 gün):{2} adet,", ExecuteDate.Month, ExecuteDate.Year, AcikHataOnem1));
            sb.Append(string.Format(" Acik Hata (Onem=2)(Yaş>30 gün):{0} adet,", AcikHataOnem2));
            sb.Append(string.Format(" Açık Hata Yaşı Metriği:{0} adet", Result));

            if (!t)
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            }
            else
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            }


            return sb.ToString();
        }
    }
}
