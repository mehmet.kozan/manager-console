﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// kullanım durumu trackerı içinde ilk, değişen, eklenen, çıkartılan, silinen statulerdeki issue sayılarını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M019 : MetricResult<M019>
    {
        [XmlElement]
        public int Ilk { get; set; }
        [XmlElement]
        public int Degisen { get; set; }
        [XmlElement]
        public int Eklenen { get; set; }
        [XmlElement]
        public int Cıkartılan { get; set; }
        [XmlElement]
        public int Silinen { get; set; }

        [XmlElement]
        public double Result { get; set; }

        
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();


            sb.Append(string.Format("Sonuc( {0}/{1} ): İlk:{2} adet Değişen:{3} adet Eklenen:{4} adet Çıkartılan:{5} adet Silinen:{6} adet", ExecuteDate.Month, ExecuteDate.Year, Ilk, Degisen, Eklenen, Cıkartılan, Silinen));

            return sb.ToString();

        }

    }
}
