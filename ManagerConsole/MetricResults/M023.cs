﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// (Hata(Önem=1) / Gereksinim( Kodlanmış + Biten)) < 0.2 oranını hesaplar. (gereksinim?)
    /// </summary>
    /// 
    [Serializable]
    public class M023 : MetricResult<M023>
    {
        [XmlElement]
        public double AcikHataOnem1 { get; set; }
        [XmlElement]
        public double AcikHataOnem2 { get; set; }
        [XmlElement]
        public double AcikHataOnem3 { get; set; }
        [XmlElement]
        public double AcikHataOnem4 { get; set; }
        [XmlElement]
        public double AcikHataOnem5 { get; set; }
        [XmlElement]
        public double Kodlanmis { get; set; }
        [XmlElement]
        public double KodlanmisGereksinim { get; set; }
        [XmlElement]
        public double BitenGereksinim { get; set; }
        [XmlElement]
        public double Biten { get; set; }


        //public double KodlanmisGereksinim { get; set; }
        //public double BitenGereksinim { get; set; }
        //public double ToplamGereksinim { get; set; }

        [XmlElement]
        public double Result { get; set; }

        public bool CalculateResult()
        {
            bool t;

            Result = AcikHataOnem1 / (KodlanmisGereksinim + BitenGereksinim);
            //ToplamGereksinim = KodlanmisGereksinim + BitenGereksinim;


            if (Result < (0.2))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = true;
            }
            else
            {
                t = false;
            }
            return t;
        }

        
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): Acik Hata (Onem=1):{2} adet,", ExecuteDate.Month, ExecuteDate.Year, AcikHataOnem1));
            sb.Append(string.Format(" Acik Hata (Onem=2):{0} adet, Acik Hata (Onem=3):{1} adet", AcikHataOnem2, AcikHataOnem3));
            sb.Append(string.Format(" Acik Hata (Onem=4):{0} adet, Acik Hata (Onem=5):{1} adet", AcikHataOnem4, AcikHataOnem5));
            sb.Append(string.Format(" Kodlanmis:{0} adet, Biten:{1} adet", Kodlanmis, Biten));
            sb.Append(string.Format(" KodlanmisGereksinim:{0} adet, BitenGereksinim:{1} adet,", KodlanmisGereksinim, BitenGereksinim));
            sb.Append(string.Format(" Açık Hata Önem Dağılımı Metriği:{0:f2}", Result));

            if (!t)
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            }
            else
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            }


            return sb.ToString();
        }
    }
}
