﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    [Serializable]
    public class M013 : MetricResult<M013>
    {
        [XmlElement]
        public int Yeni { get; set; }
        [XmlElement]
        public int DevamEdiyor { get; set; }
        [XmlElement]
        public int Cozuldu { get; set; }
        [XmlElement]
        public int Kapatildi { get; set; }
        [XmlElement]
        public int Reddedildi { get; set; }
        [XmlElement]
        public int Iptal { get; set; }
        [XmlElement]
        public int Toplam { get; set; }

        [XmlElement]
        public double Result { get; set; }
                
        public bool CalculateResult()
        {
            bool t;
            Result = (Yeni + DevamEdiyor);
            Toplam = Yeni + DevamEdiyor + Cozuldu + Kapatildi + Reddedildi + Iptal;

            if (Result < 5)
            {
                t = true;
            }
            else
            {
                t = false;
            }

            return t;
        }

        public override string ToString()
        {

            bool t = CalculateResult();
            StringBuilder sb = new StringBuilder();

            if (Toplam != 0)
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format("Yeni:{0} adet, DevamEdiyor:{1} adet, Cozuldu:{2} adet, Kapatildi:{3} adet, Reddedildi:{4} adet, Iptal:{5} adet,", Yeni, DevamEdiyor, Cozuldu, Kapatildi, Reddedildi, Iptal));
                sb.Append(string.Format(" DÖF Metriği: {0} ", Result));

                if (!t)
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli değil."));
                }

            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format(" DÖF bulunamadı. "));
            }

            return sb.ToString();

        }

    }
}
