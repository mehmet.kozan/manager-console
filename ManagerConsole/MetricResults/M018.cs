﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// RoadMap Gerçekleşme Metriği.
    /// </summary>
    /// 
    [Serializable]
    public class M018 : MetricResult<M018>
    {
        [XmlElement]
        public int Gereksinim { get; set; }
        [XmlElement]
        public int Hata { get; set; }
        [XmlElement]
        public int IslemMaddesi { get; set; }
        [XmlElement]
        public int Gorev { get; set; }
        [XmlElement]
        public int IslerCozumlenmisVeDogrulanmisIsler { get; set; }
        [XmlElement]
        public int PlanlanmamisIsler { get; set; }

  
        public bool CalculateResult()
        {
            bool t = true;

            IslerCozumlenmisVeDogrulanmisIsler = Gereksinim + Hata + IslemMaddesi + Gorev;
            PlanlanmamisIsler = Gereksinim + Hata;

            //if (Result < (0.2))
            //{
            //    t = true;
            //}
            //else if (double.IsNaN(Result))
            //{
            //    t = true;
            //}
            //else
            //{
            //    t = false;
            //}

            return t;
        }

        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): Gereksinim:{2} adet, Hata:{3} adet, İşlem Maddesi:{4} adet", ExecuteDate.Month, ExecuteDate.Year, Gereksinim, Hata, IslemMaddesi));
            sb.Append(string.Format(" Görev:{0} adet, İşler,Çözümlenmiş ve Doğrulanmış İşler:{1} adet, Planlanmamiş İşler:{2} adet", Gorev, IslerCozumlenmisVeDogrulanmisIsler, PlanlanmamisIsler));

            return sb.ToString();

        }        
    }
}
