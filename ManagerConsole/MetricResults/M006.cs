﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    [Serializable]
    public class M006 : MetricResult<M006>
    {
        [XmlElement]
        public double YesilRiskler { get; set; }

        [XmlElement]
        public double SariRiskler { get; set; }

        [XmlElement]
        public double KirmiziRiskler { get; set; }

        [XmlElement]
        public double TumRiskler { get; set; }

        [XmlElement]
        public double Result { get; set; }

        [XmlElement]
        public int Percentage { get; set; }


        public bool CalculateResult()
        {
            bool t;

            Result = KirmiziRiskler / TumRiskler;
            Percentage = (int)Math.Round(Result * 100);

            if (Result < (0.2))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = true;
            }
            else
            {
                t = false;
            }

            
            return t;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (TumRiskler != 0)
            {
                bool t = CalculateResult();

                sb.Append(string.Format("Sonuc( {0}/{1} ): ", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format("{0:f2} ", Result));
                sb.Append(string.Format("YesilRiskler:{0} adet, SariRiskler:{1} adet, KirmiziRiskler:{2} adet, TumAçıkRiskler:{3} adet,", YesilRiskler, SariRiskler, KirmiziRiskler, TumRiskler));
                sb.Append(string.Format(" Risk Renk Metriği: %{0} ", Percentage));

                if (!t)
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli değil."));
                }
            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format(" Risk bulunamadı. "));
            }


            return sb.ToString();

        }

    }
}
