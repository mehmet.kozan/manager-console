﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// Toplam MDK Sayısını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M021 : MetricResult<M021>
    {
        [XmlElement]
        public int AcikMDK { get; set; }
        [XmlElement]
        public int KapaliMDK { get; set; }
        [XmlElement]
        public int ToplamMDK { get; set; }

       
        public bool CalculateResult()
        {
            bool t;

            ToplamMDK = AcikMDK + KapaliMDK;                

            if (ToplamMDK < 10)
            {
                t = true;
            }
            else
            {
                t = false;
            }
            return t;
        }

        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();

            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): AcikMDK:{2} adet, KapaliMDK:{3} adet, ToplamMDK:{4} adet", ExecuteDate.Month, ExecuteDate.Year, AcikMDK, KapaliMDK, ToplamMDK));

            if (!t)
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            }
            else
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            }
                
          
            return sb.ToString();
        }
    }
}
