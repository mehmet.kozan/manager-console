﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// Gereksinim trackerı içinde  (Durum=İptal) + (Kaynağı=Ek Talep) / Toplam Gereksinim  oranını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M017 : MetricResult<M017>
    {
        [XmlElement]
        public double EkTalepIptal { get; set; }
        [XmlElement]
        public double Toplam { get; set; }

        [XmlElement]
        public double Result { get; set; }

        public bool CalculateResult()
        {
            bool t;

            Result = EkTalepIptal / Toplam;

            if (Result < (0.2))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = true;
            }
            else
            {
                t = false;
            }
            
            return t;
        }

      
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            if (Toplam != 0)
            {


                sb.Append(string.Format("Sonuc( {0}/{1} ): Gereksinimlerin Değişkenliği Metriği:{2:f2}, Ek Talep Iptal :{3} adet, Toplam:{4} adet", ExecuteDate.Month, ExecuteDate.Year, Result, EkTalepIptal, Toplam));

                if (!t)
                {
                    sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
                }
            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ): Ek Talep Iptal:{2} adet,", ExecuteDate.Month, ExecuteDate.Year, EkTalepIptal));
                sb.Append(string.Format(" Gereksinim bulunamadı. "));
            }

            

            return sb.ToString();

        }

    }
}
