﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    [XmlInclude(typeof(M004))]
    [XmlInclude(typeof(M005))]
    [XmlInclude(typeof(M006))]
    [XmlInclude(typeof(M010))]
    [XmlInclude(typeof(M013))]
    [XmlInclude(typeof(M014))]
    [XmlInclude(typeof(M015))]
    [XmlInclude(typeof(M016))]
    [XmlInclude(typeof(M017))]
    [XmlInclude(typeof(M018))]
    [XmlInclude(typeof(M019))]
    [XmlInclude(typeof(M020))]
    [XmlInclude(typeof(M021))]
    [XmlInclude(typeof(M022))]
    [XmlInclude(typeof(M023))]
    [XmlInclude(typeof(M024))]
    [XmlInclude(typeof(M025))]
    [XmlInclude(typeof(M026))]
    public class MetricResult<T>
    {
        [XmlAttribute]
        public string MetricName  { get; set; }

        [XmlAttribute]
        public string ProjecName { get; set; }

        [XmlAttribute]
        public DateTime ExecuteDate {get;set;}

        [XmlAttribute]
        public double TotatExecutionTime { get; set; }



        public static string Serialize<T>(T value)
        {

            if (value == null)
            {
                return null;
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = new UnicodeEncoding(false, false); // no BOM in a .NET string
            settings.Indent = true;
            settings.OmitXmlDeclaration = false;

            using (StringWriter textWriter = new StringWriter())
            {
                using (XmlWriter xmlWriter = XmlWriter.Create(textWriter, settings))
                {
                    serializer.Serialize(xmlWriter, value);
                }
                return textWriter.ToString();
            }
        }

        public static T Deserialize<T>(string xml)
        {

            if (string.IsNullOrEmpty(xml))
            {
                return default(T);
            }

            XmlSerializer serializer = new XmlSerializer(typeof(T));

            XmlReaderSettings settings = new XmlReaderSettings();
            // No settings need modifying here

            using (StringReader textReader = new StringReader(xml))
            {
                using (XmlReader xmlReader = XmlReader.Create(textReader, settings))
                {
                    return (T)serializer.Deserialize(xmlReader);
                }
            }
        }


        public string FileName 
        {
            get 
            {
                return string.Format("{0}-{1}-{2}.xml", ExecuteDate.ToString("yyyy-MM-dd_hh-mm-ss"), MetricName, ProjecName);
            
            }
        
        }

        public void SaveAsFile() 
        {


            string str = Serialize(this);


            var appDir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var filePath = System.IO.Path.Combine(appDir, FileName);

            System.IO.File.WriteAllText(filePath, str);

        
        }

    }
}
