﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// kullanım durumu trackerı içinde ilk, değişen, eklenen, çıkartılan, silinen statulerdeki issue sayılarını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M020 : MetricResult<M020>
    {
        [XmlElement]
        public double Ilk { get; set; }

        [XmlElement]
        public double Degisen { get; set; }

        [XmlElement]
        public double Eklenen { get; set; }

        [XmlElement]
        public double Cikartilan { get; set; }

        [XmlElement]
        public double Silinen { get; set; }

        [XmlElement]
        public double Toplam { get; set; }

        [XmlElement]
        public double Result { get; set; }

        public bool CalculateResult()
        {
            bool t;

            Toplam = Ilk + Degisen + Eklenen + Cikartilan + Silinen;
            Result = (Cikartilan + Silinen) + Eklenen / Toplam;


            if (Result > (0.9))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = false;
            }
            else
            {
                t = false;
            }

            return t;
        }

       
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): Çıkartılan:{2} adet Silinen:{3} adet Eklenen:{4} adet Toplam:{5} adet KD Metriği:{6} adet", ExecuteDate.Month, ExecuteDate.Year, Cikartilan, Silinen, Eklenen, Toplam, Result));

            if (!t)
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            }
            else
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            }

            return sb.ToString();

        }
    }
}
