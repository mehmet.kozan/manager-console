﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// Gereksinim trackerı içinde başlanmamış,devam eden, kodlanmış,biten statulerdeki issue sayılarını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M014 : MetricResult<M014>
    {

        [XmlElement]
        public int Baslanmamis;

        [XmlElement]
        public int DevamEden;

        [XmlElement]
        public int Kodlanmis;

        [XmlElement]
        public int Biten;

        [XmlElement]
        public double Result;

       

        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();


            sb.Append(string.Format("Sonuc( {0}/{1} ): Baslanmamış:{2} adet, Devam Eden:{3} adet, Kodlanmış:{4} adet, Biten:{5} adet", ExecuteDate.Month, ExecuteDate.Year, Baslanmamis, DevamEden, Kodlanmis, Biten));
            
            return sb.ToString();

        }

    }
}
