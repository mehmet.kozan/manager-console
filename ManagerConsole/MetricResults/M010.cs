﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    [Serializable]
    public class M010 : MetricResult<M010>
    {
        [XmlElement]
        public double GozdenGecirmeEforu { get; set; }
        [XmlElement]
        public double ToplamEfor { get; set; }
        [XmlElement]
        public double Result { get; set; }
        [XmlElement]
        public int Percentage { get; set; }


        public bool CalculateResult()
        {
            bool t;
            Result = GozdenGecirmeEforu / ToplamEfor;
            Percentage = (int)Math.Round(Result * 100);

            if (Result > (0.05))
            {
                t = true;
            }
            else if (double.IsNaN(Result))
            {
                t = false;
            }
            else
            {
                t = false;
            }

            return t;
        }

        public override string ToString()
        {

            bool t = CalculateResult();
            StringBuilder sb = new StringBuilder();

            if (ToplamEfor != 0)
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format("{0:f2} ", Result));
                sb.Append(string.Format("GozdenGecirmeEforu:{0} saat, ToplamEfor:{1} saat, ", GozdenGecirmeEforu, ToplamEfor));
                sb.Append(string.Format(" Gözden Geçirme Efor Metriği: %{0} ", Percentage));

                if (!t)
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli."));
                }
                else
                {
                    sb.Append(string.Format("Acil önlem toplantısı gerekli değil."));
                }

            }
            else
            {
                sb.Append(string.Format("Sonuc( {0}/{1} ):", ExecuteDate.Month, ExecuteDate.Year));
                sb.Append(string.Format(" Efor bulunamadı. "));
            }

            return sb.ToString();

        }

    }
}
