﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ManagerConsole.MetricResults
{
    /// <summary>
    /// (Toplam Hata Sayısı / Kodlanmış + Biten Gereksinim Sayısı)  oranını hesaplar.
    /// </summary>
    /// 
    [Serializable]
    public class M026 : MetricResult<M026>
    {
        [XmlElement]
        public double IcKaynakliHataSayisi { get; set; }
        [XmlElement]
        public double DisKaynakliHataSayisi { get; set; }
        [XmlElement]
        public double Result { get; set; }

        public bool CalculateResult()
        {
            bool t = true;


            if (DisKaynakliHataSayisi < IcKaynakliHataSayisi)
            {
                t = true;
            }
            else
            {
                t = false;
            }
            return t;
        }
        
        public override string ToString()
        {

            StringBuilder sb = new StringBuilder();
            bool t = CalculateResult();

            sb.Append(string.Format("Sonuc( {0}/{1} ): IcKaynakliHataSayisi:{2} adet DisKaynakliHataSayisi:{3} adet", ExecuteDate.Month, ExecuteDate.Year, IcKaynakliHataSayisi, DisKaynakliHataSayisi));

            if (!t)
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli."));
            }
            else
            {
                sb.Append(string.Format(" Acil önlem toplantısı gerekli değil."));
            }
            
            return sb.ToString();
        }
    }
}
