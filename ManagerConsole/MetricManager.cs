﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Redmine.Net.Api;
using Redmine.Net.Api.Types;
using System.Collections.Specialized;
using ManagerConsole.MetricResults;


namespace ManagerConsole
{
    public partial class MetricManager
    {
        public const int LIMIT = 100;
        private RedmineManager redmineManager { get; set; }
        public Dictionary<string, Action<string>> MetricDictionary { get; set; }

        public IssueManager IssueManager { get; set; }
        public ProjectManager ProjectManager { get; set; }

        public MetricManager(RedmineManager redmineManager)
        {
            this.redmineManager = redmineManager;

            IssueManager = new IssueManager(redmineManager);
            ProjectManager = new ProjectManager(redmineManager);

            MetricDictionary = new Dictionary<string, Action<string>>();

            MetricDictionary.Add("M004", M004);
            MetricDictionary.Add("M005", M005);
            MetricDictionary.Add("M006", M006);
            MetricDictionary.Add("M010", M010);
            MetricDictionary.Add("M013", M013);
            MetricDictionary.Add("M014", M014);
            MetricDictionary.Add("M015", M015);
            MetricDictionary.Add("M016", M016);
            MetricDictionary.Add("M017", M017);
            MetricDictionary.Add("M018", M018);
            MetricDictionary.Add("M019", M019);
            MetricDictionary.Add("M020", M020);
            MetricDictionary.Add("M021", M021);
            MetricDictionary.Add("M022", M022);
            MetricDictionary.Add("M023", M023);
            MetricDictionary.Add("M024", M024);
            MetricDictionary.Add("M025", M025);
            MetricDictionary.Add("M026", M026);
        }

        public void ExecuteMetric(string metricName, string projectName)
        {
            if (metricName == "*")
            {
                foreach (var item in MetricDictionary.Keys)
                {
                    Action<string> action = MetricDictionary[item];
                    action.Invoke(projectName);
                }
            }
            else
            {
                if (MetricDictionary.ContainsKey(metricName))
                {
                    Action<string> action = MetricDictionary[metricName];
                    action.Invoke(projectName);
                }
                else if (MetricDictionary.ContainsKey(metricName.ToUpper()))
                {
                    Action<string> action = MetricDictionary[metricName.ToUpper()];
                    action.Invoke(projectName);
                }
                else
                {
                    Console.Error.WriteLine("Please enter any valid metric name or \"Exit\".");
                }
            }
        }
    }
}
